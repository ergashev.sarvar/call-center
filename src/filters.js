import Vue from 'vue'


Vue.filter("clearAgentName", (val) => {
	let nameArr = val.split('@');
	return nameArr[0];
});

Vue.filter("clearPhoneNumber", (val) => {
	let number = val.slice(1);
	return number;
});

Vue.filter("formatDateSecond", (val) => {
	return val ? moment(val).format('D.MM.YYYY HH:mm:ss') : '';
});