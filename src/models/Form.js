'use strict';

import Errors from './Errors';

export default class Form {
	constructor(data) {
		this.originalData = {...data};

		for (let property in data) {
			this[property] = data[property];
		}

		this.errors = new Errors();
	}

	data() {
		let data = {};

		for (let property in this.originalData) {
			data[property] = this[property];
		}

		return data;
	}

	submit(type, url) {
		return new Promise((resolve, reject) => {
			axios[type.toLowerCase()](url, this.data())
			  .then(response => {
					this.onSuccess(response.data);
					resolve(response.data);
				},
				errors => {
					this.onFail(errors);
					reject(errors);
				});
		});
	}

	onSuccess() {

	}

	onFail(errors) {
		this.errors.record(errors);
	}

	reset() {
		for (let field in this.originalData) {
			this[field] = this.originalData[field];
		}

		this.errors.clear();
	}

	setData(data) {
		for (let property in data) {
			this[property] = data[property];
		}
	}
}