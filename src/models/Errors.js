export default class Errors {
	constructor() {
		this.errors = {};
		this.errorMessage = '';
	}

	get(field) {
		return this.errors[field] ? this.errors[field][0] : null;
	}

	has(field) {
		return this.errors.hasOwnProperty(field);
	}

	any() {
		return Object.keys(this.errors).length > 0;
	}

	record(errors) {
		this.errors = errors;
		console.log(this.errors);
	}

	clear(field) {
		this.errorMessage = '';
		if (field) {
			delete this.errors[field];
			return;
		}

		this.errors = {};
	}
}