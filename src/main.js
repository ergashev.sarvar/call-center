import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios/dist/axios.min'
import './filters'
import VueAxios from 'vue-axios';
import Multiselect from 'vue-multiselect'
import 'vue-multiselect/dist/vue-multiselect.min.css'
import datePicker from 'vue-bootstrap-datetimepicker';
import DatePicker from "vue-bootstrap-datetimepicker/src/component";
import JsonExcel from 'vue-json-excel'
import can from './can';



// let url = 'http://192.168.45.110:2000/api/';
// let url = 'http://192.168.45.110:8000/api/';
// let url = 'https://185.211.129.20:8000/api/';
// let url = 'http://localhost:8000/api/';
let url = 'https://crm24.uz:8000/api/';
// let url = 'https://185.183.243.67:8000/api/';
//let url = 'https://crm24.poytaxt.uz/api/';
// let url = 'https://poytaxt.crm24.uz/api/';

/*____________ axios Setup____________*/
axios.defaults.baseURL = url;
axios.defaults.headers.common = {
	'Accept': 'application/json',
	'Content-Type': 'application/json',
	'Authorization': 'Bearer ' + localStorage.token,
};
axios.interceptors.response.use(response => {
	 if (response.data.code && response.data.code === 401) {
		 localStorage.removeItem('token');
		 localStorage.removeItem('token_time');
		 localStorage.removeItem('refresh_token');
		 localStorage.removeItem('operator_id');
		 localStorage.removeItem('user_id');
		 setTimeout(() => {
			 localStorage.removeItem('permissions');
			 router.push('/login');
		 }, 300);
 }
	return response;
});





// make axios Global
window.axios = axios;
Vue.use(VueAxios, axios);

Vue.use(datePicker);
Vue.use(DatePicker);

Vue.mixin({methods: {can}});

Vue.config.productionTip = false;
window.Vue = Vue;
Vue.component('multiselect', Multiselect);
Vue.component('downloadExcel', JsonExcel);

new Vue({
	created(){
		this.$store.commit('setBaseUrl', url);
	},
	router,
	store,
	render: h => h(App)
}).$mount('#app');
