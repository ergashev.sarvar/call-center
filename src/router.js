import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

const router = new Router({
	mode: 'history',
	linkExactActiveClass: 'active',
	routes: [
		{
			path: '/',
			redirect: '/dashboard'
		},
		{
			path: '/dashboard',
			component: () => import('./views/Dashboard/Dashboard'),
			name: 'Dashboard'
		},
		{
			path: '/dashboards',
			component: () => import('./views/Dashboards/List'),
		},
		{
			path: '/dashboards/display-1',
			component: () => import('./views/Dashboards/display-1/index'),
		},
		{
			path: '/dashboards/display-2',
			component: () => import('./views/Dashboards/display-2/index'),
		},
		{
			path: '/dashboards/display-3',
			component: () => import('./views/Dashboards/display-3/index'),
		},
		{
			path: '/dashboards/display-4',
			component: () => import('./views/Dashboards/display-4/index'),
		},
		{
			path: '/dashboards/display-5',
			component: () => import('./views/Dashboards/display-5/index'),
		},
		{
			path: '/dashboards/display-6',
			component: () => import('./views/Dashboards/display-6/index'),
		},
		{
			path: '/dashboards/display-7',
			component: () => import('./views/Dashboards/display-7/index'),
		},
		{
			path: '/reports',
			component: () => import('./views/Reports/List'),
		},
		{
			path: '/reports/reports',
			component: () => import('./views/Reports/Reports/index'),
		},
		{
			path: '/report',
			component: () => import('./views/Report/index'),
		},
		{
			path: '/timesheet',
			component: () => import('./views/Timesheet/Timesheet'),
			name: 'TimeSheet'
		},
		{
			path: '/complaints',
			component: () => import('./views/Complaints/index'),
			name: 'Complaints'
		},
		{
			path: '/complaints/edit/:id',
			component: () => import('./views/Complaints/update'),
		},
		{
			path: '/voice-recorder',
			component: () => import('./views/IVR/IVR'),
			name: 'iVR'
		},
		{
			path: '/settings',
			component: () => import('./views/Settings/Settings'),
			name: 'Admin',
			children: [
				{
					path: 'groups',
					component: () => import('./views/Settings/groups/index'),
				},
				{
					path: 'users',
					component: () => import('./views/Settings/users/index.vue'),
				},
				{
					path: 'roles',
					component: () => import('./views/Settings/roles/index'),
				},
				{
					path: 'modes',
					component: () => import('./views/Settings/modes/index'),
				},
				{
					path: 'categories',
					component: () => import('./views/Settings/categories/index'),
				},
				{
					path: 'priorities',
					component: () => import('./views/Settings/priorities/index'),
				},
				{
					path: 'activate',
					component: () => import('./views/Settings/activation/index'),
				},
				{
					path: 'call-center',
					component: () => import('./views/Settings/callCenter/index'),
				},
				{
					path: 'conference',
					component: () => import('./views/Settings/conference/index'),
				},
				{
					path: 'localization',
					component: () => import('./views/Settings/localization/index'),
				},
				{
					path: 'newstype',
					component: () => import('./views/Settings/newstype/index'),
				},
				{
					path: 'handbook',
					component: () => import('./views/Settings/handbook/index'),
				},
				{
					path: 'recordings',
					component: () => import('./views/Settings/recordings/index'),
				},
				{
					path: 'contacts',
					component: () => import('./views/Settings/contacts/index'),
				},
				{
					path: 'greetings',
					component: () => import('./views/Settings/greetings/index'),
				},
				{
					path: 'faqCategory',
					component: () => import('./views/Settings/faqCategory/index'),
				},
				{
					path: 'deal',
					component: () => import('./views/Settings/deal/index'),
				},
				{
					path: 'department',
					component: () => import('./views/Settings/department/index'),
				},
				{
					path: 'organization',
					component: () => import('./views/Settings/organization/index'),
				},
			]
		},
		{
			path: '/ticket',
			component: () => import('./views/Ticket/Ticket'),
			name: 'Ticket',
			children: [
				{
					path: 'new',
					component: () => import('./views/Ticket/new/index'),
				},
				{
					path: 'done',
					component: () => import('./views/Ticket/done/index'),
				},
				{
					path: 'closed',
					component: () => import('./views/Ticket/closed/index'),
				},
				{
					path: 'waiting',
					component: () => import('./views/Ticket/waiting/index'),
				},
				{
					path: 'processed',
					component: () => import('./views/Ticket/processed/index'),
				},
				{
					path: 'deleted',
					component: () => import('./views/Ticket/deleted/index'),
				},
			]
		},
		{
			path: '/application',
			component: () => import('./views/Application/index'),
			name: 'Application',
		},
		{
			path: '/call',
			component: () => import('./views/Call/Call'),
			name: 'Call',
		},
		{
			path: '/Crm',
			component: () => import('./views/Crm/crm2'),
			name: 'crm',
		},
		{
			path: '/call/edit/:id',
			component: () => import('./views/Call/edit/index'),
		},
		{
			path: '/login',
			component: () => import('./views/Login/Login'),
			name: 'Login'
		},
		{
			path: '/activation',
			component: () => import('./views/Activation/Activation'),
			name: 'Activation'
		},
		{
			path: '/crash-info',
			component: () => import('./views/Settings/crash-info/index'),
		},
	]
});

router.beforeEach((to, from, next) => {

	if (localStorage.getItem('token')) {
		if (to.path === '/login') {
			next(from.fullPath);
		} else {
			next();
		}
	} else {
		if (to.path !== '/login') {
			next('/login');
		} else {
			next();
		}
	}


	if (localStorage.getItem('activated') === null) {
		axios.get('/Login/IsActivate')
			.then(response => {
				if (response.data.success) {
					localStorage.activated = 1;
					next();
				} else {
					if (to.path !== '/activation') {
						next('/activation');
					} else {
						next();
					}
				}
			})
			.catch(error => {
				console.log(error.message);
			});
	}
});

export default router;
