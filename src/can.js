const can = function (mod, perm) {
	let permissions = localStorage.getItem('permissions') ? JSON.parse(localStorage.getItem('permissions')) : [];
	let access = false;
	permissions.forEach(item => {
		if (item.item === mod && item.action === perm) {
			access = true;
			return;
		}
	});
	return access;
};

export default can;