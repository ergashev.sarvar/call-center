import Datepicker from "vue-bootstrap-datetimepicker";
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';

export default {
	name: "index",
	components: {Datepicker},
	data() {
		return {
			orgNumber: 0,
			options: {
				format: 'YYYY-MM-DD',
				useCurrent: false,
				showClear: true,
				showClose: true,
				locale: 'ru',
				icons: {
					time: 'mdi mdi-clock-outline',
					date: 'mdi mdi-calendar',
					up: 'mdi mdi-arrow-up',
					down: 'mdi mdi-arrow-down',
					previous: 'mdi mdi-arrow-left',
					next: 'mdi mdi-arrow-right',
					today: 'mdi mdi-calendar-today',
					clear: 'mdi mdi-delete-outline',
					close: 'mdi mdi-alpha-x-circle-outline'
				}
			},
			fromDate: null,
			toDate: null,
			income: [],
			outcome: [],
			orgList: [],
			json_fields: {
				'Оператор': 'operatorId',
				'Общая продолжительность (мин)': 'successTime',
				'Среднее время (сек)': 'avgTime',
				'Принятые звонки': 'successPcs',
				'Пропущенные звонки': 'failPcs'
			},
			json_data_income: [],
			json_data_outcome: [],
			json_meta: [
				[
					{
						'key': 'charset',
						'value': 'utf-8'
					}
				]
			],
		}
	},
	methods: {
		getIncome() {
			this.$http.get('/ClientCard/reportIncome',
				{
					params: {
						orgNumber: this.orgNumber,
						fromDate: this.fromDate,
						toDate: this.toDate
					}
				}
			).then(res => {
				this.income = res.data;
			})
		},
		getOutCome() {
			this.$http.get('/ClientCard/reportOutcome', {
				params: {
					orgNumber: this.orgNumber,
					fromDate: this.fromDate,
					toDate: this.toDate
				}
			}).then(res => {
				this.outcome = res.data;
			})
		},
		getOrg() {
			this.$http.get('/Admin/organization').then(res => {
				this.orgList = res.data.data;
			})
		},
		filter() {
			this.getOutCome();
			this.getIncome();
		},
		sumOfItems(arr, num) {
			let output = 0;
			if (num == 1) {
				arr.forEach(ar => {
					output += ar.successTime
				});
			}
			if (num == 2) {
				arr.forEach(ar => {
					output += ar.avgTime
				});
			}
			if (num == 3) {
				arr.forEach(ar => {
					output += ar.successPcs
				});
			}
			if (num == 4) {
				arr.forEach(ar => {
					output += ar.failPcs
				});
			}
			return output
		},
		fetchIncome(income) {
			for (let item of income) {
				this.json_data_income.push({
					'operatorId': item.operatorId,
					'successTime': item.successTime,
					'avgTime': item.avgTime,
					'successPcs': item.successPcs,
					'failPcs': item.failPcs
				});
			}
			this.json_data_income.push({
				'operatorId': 'Total',
				'successTime': this.sumOfItems(this.income, 1),
				'avgTime': this.sumOfItems(this.income, 2),
				'successPcs': this.sumOfItems(this.income, 3),
				'failPcs': this.sumOfItems(this.income, 4)
			})
		},
		fetchOutcome() {
			for (let item of this.outcome) {
				this.json_data_outcome.push({
					'operatorId': item.operatorId,
					'successTime': item.successTime,
					'avgTime': item.avgTime,
					'successPcs': item.successPcs,
					'failPcs': item.failPcs
				});
			}
			this.json_data_outcome.push({
				'operatorId': 'Total',
				'successTime': this.sumOfItems(this.outcome, 1),
				'avgTime': this.sumOfItems(this.outcome, 2),
				'successPcs': this.sumOfItems(this.outcome, 3),
				'failPcs': this.sumOfItems(this.outcome, 4)
			})
		}

	},
	created() {
		this.getOrg();
		this.getIncome();
		this.getOutCome();
	},
	computed: {},
	watch: {
		'income'(val) {
			this.fetchIncome(val);
		},
		'outcome'(val){
			this.fetchOutcome(val);
		}
	}
}
