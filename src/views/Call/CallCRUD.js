export default {
	Create(model) {
		return new Promise((result, reject) => {
			axios.post('/ClientCard/clientCard', model)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetUsers() {
		return new Promise((result, reject) => {
			axios.get('/Admin/users')
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetCategories() {
		return new Promise((result, reject) => {
			axios.get('/Admin/category')
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetPriorities() {
		return new Promise((result, reject) => {
			axios.get('/Admin/priority')
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetHistoryClient(operator_id, page) {
		return new Promise((result, reject) => {
			axios.get('ClientCard/callHistoryClient/' + operator_id + '/' + page)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetHistoryOperator(operator_id, page) {
		return new Promise((result, reject) => {
			axios.get('ClientCard/callHistoryUser/' + operator_id + '/' + page)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetDistrict(cityId) {
		return new Promise((result, reject) => {
			axios.get('Hokimiyat/District/' + cityId)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetStreet(districtId) {
		return new Promise((result, reject) => {
			axios.get('Hokimiyat/Street/' + districtId)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetHouse(streetId) {
		return new Promise((result, reject) => {
			axios.get('Hokimiyat/House/' + streetId)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetFlat(houseId) {
		return new Promise((result, reject) => {
			axios.get('Hokimiyat/Flat/' + houseId)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetCommunalInfo(number, branch) {
	// GetCommunalInfo(number) {
		return new Promise((result, reject) => {
			axios
				.get('Hokimiyat/GetUtilities/' + number + '/' + branch)
				// .get('Hokimiyat/GetUtilities/' + number)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},
}