import VueTags from "vue-tags";
import CKEditor from '@ckeditor/ckeditor5-vue';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Paginate from 'vuejs-paginate';
import CRUD from "./CRUD";
import Form from '@/models/Form.js';


export default {
	components: {
		VueTags,
		ckeditor: CKEditor.component,
		Paginate,
	},
	data() {
		return {
			editor: ClassicEditor,
			editorConfig: {
				// The configuration of the editor.
			},


			callModel: new Form({
				id: 0,
				phoneNumber: null,
				clientId: null,
				infoData: null,
				categoryId: '',
				categoryIds: [],
				tags: [],
				notes: '',
				operatorUserId: 0,
				executor1Id: 0,
				executor2Id: 0,
				executorCopyId: 0,
				priorityId: 0,
				deadline: null,
				status: 0,
				dateTime: null,
				cityId: 0,
				regionId: 0,
				streetId: 0,
				houseId: 0,
				flatId: 0,
				applicantName: null,
				applicantAddress: null,
				applicantNumber: null,
				clientCardDivider: null,
			}),
			noteList: [],


			categoryList: [],
			priorityList: [],



			cityList: [
				{
					id: 1,
					name: 'Tashkent'
				},
				{
					id: 2,
					name: 'Termez'
				},
				{
					id: 3,
					name: 'Bukhara'
				},
				{
					id: 4,
					name: 'Samarkand'
				},
			],
			districtList: [],
			streetList: [],
			houseList: [],
			flatList: [],
			communalsList: new Form({
				suvsoz: {
					elements: [],
					success: false,
				},
				teploEnergo: {
					elements: [],
					success: false,
				},
				uzbekEnergo: {
					elements: [],
					success: false,
				},
				uzTransGaz: {
					elements: [],
					success: false,
				},
				maxsusTrans: {
					elements: [],
					success: false,
				},
			}),
			objectInfo: {},


			clientHistoryList: [],
			clientHistoryListPageCount: 0,
		}
	},
	methods: {
		Save() {
			this.callModel.categoryIds = this.callModel.categoryIds.length > 0 ? this.callModel.categoryIds.map(x => x.id.toString()) : [];
			this.callModel.priorityId = this.callModel.priorityId === null ? 0 : this.callModel.priorityId.id;

			// console.log(this.callModel.data());

			this.callModel.submit('patch', '/ClientCard/clientCard').then(result => {
				// console.log(result);

				if (result.success) {
					this.$router.push('/ticket');
				}
				else {
					console.log(result.message);
				}
			});
		},


		GetPlaces() {
			CRUD.GetDistrict(this.callModel.cityId).then(result => {
				// console.log(result);

				if (result.success) {
					this.districtList = result.districts;
				}
				else {
					console.log(result.message);
				}
			});
			CRUD.GetStreet(this.callModel.regionId).then(result => {
				// console.log(result);

				if (result.success) {
					this.streetList = result.streets;
				}
				else {
					console.log(result.message);
				}
			});
			CRUD.GetHouse(this.callModel.streetId).then(result => {
				// console.log(result);

				if (result.success) {
					this.houseList = result.houses;
				}
				else {
					console.log(result.message);
				}
			});
			CRUD.GetFlat(this.callModel.houseId).then(result => {
				// console.log(result);

				if (result.success) {
					this.flatList = result.flats;
				}
				else {
					console.log(result.message);
				}
			});
		},



		GetHistoryClient(page = 1) {
			CRUD.GetHistoryClient(this.callModel.phoneNumber, page).then(result => {
				// console.log(result);

				this.clientHistoryList = result.callHistoryResponses;
				this.clientHistoryListPageCount = Math.ceil(result.count / 10);

				window.scroll({
					top: 0,
					left: 0,
					behavior: 'smooth'
				});
			});
		},

		AudioPlay(event) {
			let audio = document.querySelectorAll('.call_discuss_rec_play audio');
			for (let i = 0; i < audio.length; i++) {
				if (audio[i] === event.target) continue;
				audio[i].pause();
			}
		},
	},
	mounted() {

		CRUD.Read(this.$route.params.id).then(result => {
			// console.log(result);

			if (result.success) {
				if (result.clientCard.tags.length === 1 && result.clientCard.tags[0] === '') {
					result.clientCard.tags = [];
				}

				result.clientCard.notes = '';
				this.callModel.setData(result.clientCard);
				if (result.clientCard.infoData && JSON.parse(result.clientCard.infoData).hasOwnProperty('objectInfo')) {
					this.objectInfo = JSON.parse(result.clientCard.infoData).objectInfo;
					this.communalsList.setData(JSON.parse(result.clientCard.infoData).communalsList);
				}


				this.noteList = result.notes;
				this.categoryList = result.categories;
				this.priorityList = result.priorities;


				if (this.callModel.categoryIds.length > 0 && this.callModel.categoryIds[0] !== '') {
					let tempArr = [];
					this.callModel.categoryIds.forEach((item, index, array) => {
						tempArr.push({
							id: parseInt(item),
							name: this.categoryList.find(x => x.id === parseInt(item)).name
						});
					});
					this.callModel.categoryIds = tempArr;
				}
				else {
					this.callModel.categoryIds = [];
				}


				if (this.priorityList.find(x => x.id === this.callModel.priorityId)) {
					this.callModel.priorityId = {
						id: this.callModel.priorityId,
						name: this.priorityList.find(x => x.id === this.callModel.priorityId).name
					};
				}
				else {
					this.callModel.priorityId = null;
				}


				this.GetPlaces();
				this.GetHistoryClient();
			}
			else {
				console.log(result.message);
			}
		});
	},
}