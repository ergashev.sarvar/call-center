// import VueTags from "vue-tags";
import CKEditor from '@ckeditor/ckeditor5-vue';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Paginate from 'vuejs-paginate';
import CallCRUD from './CallCRUD';
import Form from '@/models/Form.js';


export default {
	components: {
		// VueTags,
		ckeditor: CKEditor.component,
		Paginate,
	},
	data() {
		return {

			allDistricts: [],
			listOfOrg: [],

			branch: 0,

			tableArr: [],

			page: 1,
			allCount: null,
			newsType: [],

			news: [],
			editor: ClassicEditor,
			editorConfig: {},


			callModel: new Form({
				phoneNumber: null,
				clientId: null,
				infoData: null,
				categoryIds: [],
				tags: [],
				notes: '',
				operatorUserId: 0,
				executor1Id: 0,
				executor2Id: 0,
				executorCopyId: 0,
				priorityId: null,
				deadline: null,
				cityId: 0,
				regionId: 0,
				streetId: 0,
				houseId: 0,
				flatId: 0,
				applicantName: null,
				applicantAddress: null,
				applicantNumber: null,
				clientCardDivider: null,
			}),


			categoryList: [],
			priorityList: [],


			cityList: [
				{
					id: 1,
					name: 'Tashkent'
				},
			],
			districtList: [],
			allStreets: [],
			streetList: [],
			houseList: [],
			flatList: [],

			communalsList: new Form({
				suvsoz: {
					elements: [],
					success: false,
				},
				teploEnergo: {
					elements: [],
					success: false,
				},
				uzbekEnergo: {
					elements: [],
					success: false,
				},
				uzTransGaz: {
					elements: [],
					success: false,
				},
				maxsusTrans: {
					elements: [],
					success: false,
				},
			}),
			objectInfo: {},


			clientHistoryList: [],
			clientHistoryListPageCount: 0,
			operatorHistoryList: [],
			operatorHistoryListPageCount: 0,
		}
	},
	methods: {

		createTableFunc() {
			let htmlTable = this.communalsList.uzbekEnergo.elements.find(x => x.key === 'vitrinaInfo').value
			let emptyArr = [];
			let table = htmlTable.split('</table>')[0];
			table = table.split('<tr>');
			table.splice(0, 3);
			table.forEach((str, index) => {
				str = str.split('>&nbsp;<').join('><');
				// str = str.split('><').join('>&nbsp;<');
				table[index] = str.split('>')
			})
			table.forEach((arr) => {
				arr.forEach((str, index, innerArr) => {
					innerArr[index] = str.split('<')[0]
				})
				arr.splice(0, 1);
			})
			table.forEach((arr) => {
				let newObj = {};
				arr.forEach((str, index) => {
					let name;
					if (index === 0) {
						name = 'date'
					} else if (index === 1) {
						name = 'action'
					} else if (index === 2) {
						name = 'meterReadings'
					} else if (index === 3) {
						name = 'difference'
					} else if (index === 4) {
						name = 'kw'
					} else if (index === 5) {
						name = 'pricing'
					} else if (index === 6) {
						name = 'accrued'
					} else if (index === 7) {
						name = 'payment'
					} else if (index === 8) {
						name = 'debt'
					} else if (index === 9) {
						name = 'prepayment'
					}
					newObj[name] = str
				});
				emptyArr.push(newObj);
			});
			this.tableArr = emptyArr;
		},


		paginateData(page) {
			this.page = page;
			this.getNews();
		},

		getNews() {
			this.$http.get('HokimiyatNews/newsTypes').then(res => {
				this.newsType = res.data.newsTypes;
			});
			this.$http.get('Admin/organization').then(res => {
				this.listOfOrg = res.data.data;
			});
			/*let cityId = this.callModel.cityId === 0 ? 0 : this.callModel.cityId.id;
			let regionId = this.callModel.regionId === 0 ? 0 : this.callModel.regionId.id;
			let streetId = this.callModel.streetId === 0 ? 0 : this.callModel.streetId.id;
			let houseId = this.callModel.houseId === 0 ? 0 : this.callModel.houseId.id;
			let flatId = this.callModel.flatId === 0 ? 0 : this.callModel.flatId.id;*/

			let cityId = !this.callModel.cityId ? 0 : this.callModel.cityId.id;
			let regionId = !this.callModel.regionId ? 0 : this.callModel.regionId.id;
			let streetId = !this.callModel.streetId ? 0 : this.callModel.streetId.id;
			let houseId = !this.callModel.houseId ? 0 : this.callModel.houseId.id;
			let flatId = !this.callModel.flatId ? 0 : this.callModel.flatId.id;
			let from = moment().subtract(7, 'd').format('YYYY-MM-DD');
			let to = moment(new Date()).format('YYYY-MM-DD');
			this.$http.get('/HokimiyatNews/newsFiltered', {
				params: {
					cityId: cityId,
					regionId: regionId,
					streetId: streetId,
					houseId: houseId,
					page: this.page,
					userId: this.$store.state.user_id,
					isFinished: false,
					from: from,
					to: to
				}
			}).then(res => {
				if (res.data) {
					this.news = res.data.newses;
					this.allCount = res.data.count;
				}
			});
		},
		Save() {
			this.callModel.phoneNumber = this.$store.state.caller_id;
			this.callModel.clientId = this.$store.state.caller_id;
			this.callModel.operatorUserId = this.$store.state.user_id;


			this.callModel.infoData = JSON.stringify({
				objectInfo: this.objectInfo,
				communalsList: this.communalsList.data(),
			});


			this.callModel.categoryIds = this.callModel.categoryIds.length > 0 ? this.callModel.categoryIds.map(x => x.id.toString()) : [];
			this.callModel.priorityId = this.callModel.priorityId === null ? 0 : this.callModel.priorityId.id;

			this.callModel.cityId = this.callModel.cityId === null ? 0 : this.callModel.cityId.id;
			this.callModel.regionId = this.callModel.regionId === null ? 0 : this.callModel.regionId.id;
			this.callModel.streetId = this.callModel.streetId === null ? 0 : this.callModel.streetId.id;
			this.callModel.houseId = this.callModel.houseId === null ? 0 : this.callModel.houseId.id;
			this.callModel.flatId = this.callModel.flatId === null ? 0 : this.callModel.flatId.id;


			this.callModel.submit('post', '/ClientCard/clientCard').then(result => {

				if (result.success) {
					this.callModel.reset();
					this.communalsList.reset();
					this.objectInfo = {};

					this.$store.state.notifySave();
				}
				else {
					console.log(result.message);
				}
			});
		},


		SelectTree2(selected, type) {
			console.log(type);

			switch (type) {
				case 'city':
					this.callModel.regionId = null;
					this.callModel.streetId = null;
					this.callModel.houseId = null;
					this.callModel.flatId = null;

					this.districtList = [];
					this.streetList = [];
					this.houseList = [];
					this.flatList = [];

					this.objectInfo = {};

					this.getNews();

					if (selected !== null) {
						CallCRUD.GetDistrict(selected.id).then(result => {

							if (result.success) {
								this.districtList = result.districts;
							}
							else {
								console.log(result.message);
							}
						});

					}
					break;


				case 'district':
					this.callModel.streetId = null;
					this.callModel.houseId = null;
					this.callModel.flatId = null;
					//delete
					this.branch = selected.branch
					//delete
					this.streetList = [];
					this.houseList = [];
					this.flatList = [];

					this.objectInfo = {};

					this.getNews();

					if (selected !== null) {
						CallCRUD.GetStreet(selected.id).then(result => {

							if (result.success) {
								this.streetList = result.streets;
							}
							else {
								console.log(result.message);
							}
						});
					}
					break;


				case 'street':
					this.callModel.houseId = null;
					this.callModel.flatId = null;

					this.houseList = [];
					this.flatList = [];

					this.objectInfo = {};

					this.getNews();

					if (selected !== null) {
						CallCRUD.GetHouse(selected.id).then(result => {

							if (result.success) {
								this.houseList = result.houses;
							}
							else {
								console.log(result.message);
							}
						});
					}
					break;


				case 'house':
					this.callModel.flatId = null;

					this.flatList = [];

					this.objectInfo = {};

					this.getNews();

					if (selected !== null) {
						this.objectInfo = {
							elevatorCount: selected.elevatorCount,
							flatsCount: selected.flatsCount,
							floorsCount: selected.floursCount,
							porchCount: selected.porchCount,
							roomsCount: selected.roomsCount,
							tchsjName: selected.tchsjName,
							wallType: selected.wallType,
						};

						CallCRUD.GetFlat(selected.id).then(result => {

							if (result.success) {
								this.flatList = result.flats;
							}
							else {
								console.log(result.message);
							}
						});
					}
					break;


				case 'flat':
					this.objectInfo.cadastreNumber = null;
					this.objectInfo.area = null;

					this.getNews();

					if (selected !== null) {
						this.objectInfo.cadastreNumber = selected.cadastreNumber;
						this.objectInfo.area = selected.area;

						CallCRUD.GetCommunalInfo(selected.cadastreNumber, this.branch).then(result => {
							// CallCRUD.GetCommunalInfo(selected.cadastreNumber).then(result => {
							this.communalsList.setData(result);
							this.createTableFunc();
						});
					}
					break;
			}
		},


		GetHistoryClient(page = 1) {
			CallCRUD.GetHistoryClient(this.$store.state.caller_id, page).then(result => {

				this.clientHistoryList = result.callHistoryResponses;
				this.clientHistoryListPageCount = Math.ceil(result.count / 10);

				window.scroll({
					top: 0,
					left: 0,
					behavior: 'smooth'
				});
			});
		},
		GetHistoryOperator(page = 1) {
			CallCRUD.GetHistoryOperator(this.$store.state.operator_id, page).then(result => {


				this.operatorHistoryList = result.callHistoryResponses;
				this.operatorHistoryListPageCount = Math.ceil(result.count / 10);

				window.scroll({
					top: 0,
					left: 0,
					behavior: 'smooth'
				});
			});
		},

		AudioPlay(event) {
			let audio = document.querySelectorAll('.call_discuss_rec_play audio');
			for (let i = 0; i < audio.length; i++) {
				if (audio[i] === event.target) continue;
				audio[i].pause();
			}
		},
		getViewStreets(ids) {

			let result = '';
			ids.forEach(id => {
				result += ((this.allStreets.find(item => item.id === parseInt(id)) || {name: ''}).name + ', ');
			});
			return result.slice(0, -2);
		},
		getOrgById(id) {
			let list = this.listOfOrg.find(x => x.id === id);
			return list ? list.name : '---'
		},
		getDisById(id) {
			let list = this.allDistricts.find(x => x.id === id);
			return list ? list.name : '---';
		}
	},
	created() {
		this.$http.get('/Hokimiyat/District/0').then(res => {
			this.allDistricts = res.data.districts;
		});
		this.$http.get('/Hokimiyat/Streets').then(res => {
			this.allStreets = res.data.streets;
		})
	},
	mounted() {
		this.getNews();
		CallCRUD.GetCategories().then(result => {

			if (result.success) {
				this.categoryList = result.data;
			}
			else {
				console.log(result.message);
			}
		});

		CallCRUD.GetPriorities().then(result => {

			if (result.success) {
				this.priorityList = result.data;
			}
			else {
				console.log(result.message);
			}
		});


		this.callModel.applicantNumber = ((this.$store.state.caller_id !== null) && (this.$store.state.caller_id.length > 4)) ? this.$store.state.caller_id.slice(1) : this.$store.state.caller_id;


		this.GetHistoryClient();
		this.GetHistoryOperator();


		setTimeout(() => {
			document.querySelector('.ck-content').tabIndex = 9;
		}, 1000);
	},
	watch: {
		'$store.state.call_terminated'(val) {
			if (val) {
				this.GetHistoryClient();
				this.GetHistoryOperator();
				this.$store.state.call_terminated = false;
			}
		},
		'$store.state.caller_id'(val) {
			this.callModel.applicantNumber = val;
		},
	},
	computed: {
		lang() {
			return this.$store.state.lang
		},
		pagesCount: function () {
			return Math.ceil(this.allCount / 10);
		},
		filteredTable() {
			let newArr = [];
			let tableArr = this.tableArr;
			for (let i = 0, arrLength = tableArr.length; i <= arrLength; i++) {
				if (tableArr[i].payment) {
					for (let n = 0; n <= arrLength; n++) {
						if (tableArr[n].meterReadings) {
							newArr.push({date: tableArr[i].date, meterReadings: tableArr[n].meterReadings, payment: tableArr[i].payment})
							break;
						}
					}
				}
				if (newArr.length === 1) {
					break;
				}
			}
			return newArr;
		}
	}
}