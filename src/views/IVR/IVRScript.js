import '@/../public/lib/treant/Treant.js';
import Form from "../../models/Form";

export default {
	components: {},
	data() {
		return {
			fileName: '',
			emptyMenu: {
				text: {name: ""},
				type: 'empty',
				HTMLclass: "add_new",
				innerHTML: "<div data-toggle='modal' data-target='#addMenu' ><i class='mdi mdi-plus'></i></div>"
			},
			emptyEntry: {
				text: {name: ""},
				type: 'empty',
				HTMLclass: "add_new child",
				innerHTML: "<div data-toggle='modal' data-target='#addEntry' ><i class='mdi mdi-plus'></i></div>",
				children: []
			},
			chart_config: {
				chart: {
					container: "#tree",
					levelSeparation: 50,
					rootOrientation: "WEST",
					nodeAlign: "CENTER",
					connectors: {
						type: "step",
						style: {
							"stroke": "#52E2BB",
							"stroke-width": 3,
							'arrow-end': 'oval-wide-long'
						}
					}
				},

				nodeStructure: {
					text: {name: ""},
					HTMLclass: "add_new",
					innerHTML: "<div data-toggle='modal' data-target='#addMenu'><i class='mdi mdi-plus'></i></div>",
					children: [],
				}
			},
			form: new Form({
				id: null,
				name: '',
				greetLong: '',
				greetShort: '',
				exitSound: '',
				invalidSound: '',
				confirmMacro: '',
				confirmKey: '',
				ttsEngine: '',
				ttsVoice: '',
				confirmAttempts: '',
				timeout: '',
				interDigitTimeout: '',
				maxFailures: '',
				maxTimeouts: '',
				digitLen: '',
				ivrId: null,
			}),
			entryForm: new Form({
				id: null,
				action: "",
				digits: "",
				param: "",
				ivrMenuId: null
			}),
			createNewIvr: new Form({
				name: ''
			}),
			menus: [],
			treeConfigs: [],
			wantEdit: false,
			ivrCategories: null,
			ivrSelectedId: 'all',
			selectedCategoryToDelete: null,
			selectedCategoryToEdit: {name: ''}
		}
	},
	mounted() {
		let _this = this;
		$(document).on('click', '.node', function (e) {
			let type = $(this).find('div').data('type');
			let id = $(this).find('div').data('id');
			let p_id = $(this).find('div').data('parent');
			if (!id) {
				_this.entryForm.reset();
				delete _this.entryForm.id;
				_this.entryForm.ivrMenuId = parseInt(p_id);
				_this.form.reset();
				delete _this.form.id;
				_this.wantEdit = false;
				return;
			}
			_this.wantEdit = true;
			if (type === 'child') {
				_this.form.submit('get', '/IvrEntry/entry/' + id)
				  .then(response => {
					  _this.entryForm.setData(response.data);
				  })
			} else {
				_this.form.submit('get', '/IvrMenu/menu/' + id)
				  .then(response => {
					  _this.form.setData(response.data);
				  });
			}
		});
		new Treant(this.chart_config);
	},
	methods: {
		runTreant() {
			setTimeout(() => {
				for (let i = 0; i < this.treeConfigs.length; i++) {
					new Treant(this.treeConfigs[i]);
				}
			}, 1000);
		},
		submitForm() {
			let type = this.wantEdit ? 'put' : 'post';
			if (this.ivrSelectedId === 'all') {
				let item = this.menus.find(obj => {
					return obj.id === this.form.id
				})
				this.form.ivrId = item.ivrId
			} else {
				this.form.ivrId = this.ivrSelectedId;
			}
			this.form.submit(type, '/IvrMenu/menu')
			  .then(response => {
				  if (this.wantEdit) {
					  let id = this.form.id;
					  let menu = this.treeConfigs.find(item => item.id === id);
					  menu.nodeStructure.innerHTML = "<div data-toggle='modal' data-type='parent' data-id='" + this.form.id + "' data-target='#addMenu'>" + this.form.name + "</div>";
					  this.$store.state.notifyEdit()
					  $('#addMenu').modal('hide')
				  } else {
					  let object = Object.assign({}, this.chart_config);
					  object.nodeStructure.HTMLclass = "";
					  object.nodeStructure.text.name = this.form.name;
					  object.nodeStructure.innerHTML = "<div data-toggle='modal' data-id='" + response.id + "' data-target='#addMenu'>" + this.form.name + "</div>";
					  this.emptyEntry.innerHTML = "<div data-toggle='modal' data-parent='" + response.id + "' data-target='#addEntry' ><i class='mdi mdi-plus'></i></div>";
					  object.nodeStructure.children = [this.emptyEntry];
					  object.ivrId = this.ivrSelectedId
					  let newTreeConfigs = [...this.treeConfigs]
					  newTreeConfigs = newTreeConfigs.splice(-1, 0, object)
					  this.treeConfigs = newTreeConfigs
					  this.$store.state.notifyAdd();
					  $('#addMenu').modal('hide')
				  }
				  this.renderMenus();
			  })
		},
		submitEntry() {
			let type = this.wantEdit ? 'put' : 'post';
			this.entryForm.submit(type, '/IvrEntry/entry')
			  .then(response => {
				  let parent = this.treeConfigs.find(item => item.id === this.entryForm.ivrMenuId);
				  if (this.wantEdit) {
					  let child = parent.nodeStructure.children.find(item => item.id === this.entryForm.id);
					  child.innerHTML = "<div data-toggle='modal' data-type='child' data-id='" + this.entryForm.id + "' data-target='#addEntry'>" + this.entryForm.digits + "</div>";
                      $('#addEntry').modal('hide');
				  } else {
					  let n_obj = {
						  text: {name: this.entryForm.digits},
						  HTMLclass: "child",
						  innerHTML: "<div data-toggle='modal' data-type='child' data-id='" + response.id + "' data-target='#addEntry'>" + this.entryForm.digits + "</div>"
					  };
					  if (parent.nodeStructure.children.length > 1) {
						  parent.nodeStructure.children.splice(-1, 0, n_obj);
					  } else {
						  parent.nodeStructure.children.unshift(n_obj);
					  }
					  $('#addEntry').modal('hide');
				  }
				  this.runTreant();
				  this.entryForm.reset();
                  this.renderMenus();
			  });
		},
		deleteEntry() {
			let parent = this.treeConfigs.find(item => item.id === this.entryForm.ivrMenuId);
			this.entryForm.submit('delete', '/IvrEntry/entry/' + this.entryForm.id)
			  .then(res => {
				  let item = parent.nodeStructure.children.find(item => item.id === this.entryForm.id);
				  let indx = parent.nodeStructure.children.indexOf(item);
				  parent.nodeStructure.children.splice(indx, 1);
				  this.$store.state.notifyDelete();
				  $('#addEntry').modal('hide');
				  this.runTreant();
				  this.renderMenus();
			  })
		},
		deleteMenu() {
			let item = this.treeConfigs.find(item => item.id === this.form.id);
			let indx = this.treeConfigs.indexOf(item);
			console.log(indx)
			this.form.submit('delete', '/IvrMenu/menu/' + this.form.id)
			  .then(res => {
				  this.treeConfigs.splice(indx, 1);
				  this.$store.state.notifyDelete();
				  $('#addMenu').modal('hide');
				  this.renderMenus();
			  })
		},
		createNewIvrForm() {
			this.createNewIvr.submit('post', '/IvrAdmin/ivr')
				.then(res => {
					this.ivrCategories.push({
						id: res.id,
						name: this.createNewIvr.name
					});
					this.createNewIvr.name = '';
					$('#createNewIvr').modal('hide');
				});
		},
		selectedCategoryFuncToEdit(selectedCategory) {
			this.selectedCategoryToEdit = selectedCategory
			$('#editIvr').modal('show');
		},
		editIvrCategory() {
			axios
				.put ('/IvrAdmin/ivr/', this.selectedCategoryToEdit)
				.then(response => {
					if (response.data.success === true) {
						this.getIvrCategories();
						this.selectedCategoryToEdit = {name: ''}
						$('#editIvr').modal('hide');
						this.$store.state.notifyEdit();
					}
				})
				.catch(error => {
					console.log(error);
				})
		},
		selectedCategoryFunc(selectedCategory) {
			this.selectedCategoryToDelete = selectedCategory
			$('#deleteIvr').modal('show');
		},
		deleteIvrCategory() {
			axios
				.delete ('/IvrAdmin/ivr/' + this.selectedCategoryToDelete.id)
				.then(response => {
					this.ivrCategories = this.ivrCategories.filter(obj => {
						return obj.id !== this.selectedCategoryToDelete.id
					})
					this.selectedCategoryToDelete = null
					$('#deleteIvr').modal('hide');
					this.$store.state.notifyDelete();
				})
				.catch(error => {
					console.log(error);
				})
		},
		getIvrCategories() {
			axios
				.get('/IvrAdmin/ivr')
				.then(response => {
					this.ivrCategories = response.data.data
				})
				.catch(error => {
					console.log(error);
				})
		},
		renderMenus() {
			this.form.submit('get', '/IvrMenu/menu')
				.then(response => {
						this.menus = response.data;
						response.data.forEach(item => {
							let config = {
								id: item.id,
								chart: {
									container: "#tree" + item.id + 1,
									levelSeparation: 50,
									rootOrientation: "WEST",
									nodeAlign: "CENTER",
									connectors: {
										type: "step",
										style: {
											"stroke": "#52E2BB",
											"stroke-width": 3,
											'arrow-end': 'oval-wide-long'
										}
									}

								},
								nodeStructure: {
									text: {name: item.name},
									HTMLclass: "",
									innerHTML: "<div data-toggle='modal' data-type='parent' data-id='" + item.id + "' data-target='#addMenu'>" + item.name + "</div>",
									children: [],
								}
							};
							let empty = Object.assign({}, this.emptyEntry);
							empty.innerHTML = "<div data-toggle='modal' data-parent='" + item.id + "' data-target='#addEntry' ><i class='mdi mdi-plus'></i></div>";
							if (item.entry) {
								item.entry.forEach(item => {
									config.nodeStructure.children.push({
										id: item.id,
										text: {name: item.digits},
										HTMLclass: "child",
										innerHTML: "<div data-toggle='modal' data-type='child' data-id='" + item.id + "' data-target='#addEntry'>" + item.digits + "</div>",
										children: []
									})
								});
								config.nodeStructure.children.push(empty);
							} else {
								config.nodeStructure.children = [empty];
							}
							this.entryForm.ivrMenuId = item.id;
							this.treeConfigs.push(config);
						});
					}
				)
		}
	},
	created() {
		this.renderMenus();
		this.getIvrCategories();
	},
	watch: {
		treeConfigs: function (val) {
			this.runTreant();
		},
		ivrSelectedId: function (val) {
			this.menus = []
			this.renderMenus()
		}
	}
}