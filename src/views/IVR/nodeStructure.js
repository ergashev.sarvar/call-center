const nodeStructure = {
	innerHTML: "<div class='item_name'>Menu <audio controls><source src='source/audio.mp3' type='audio/mp3'></audio></div>",
	children: [
		{
			text: {name: "Account"},
			stackChildren: true,
			connectors: {
				style: {
					'arrow-end': 'oval-wide-long'
				}
			},
			children: [
				{
					text: {name: "Receptionist"},
					HTMLclass: "reception",
					children: []
				},
				{
					text: {name: "Author"},
					children: []
				},
				{
					text: {name: ""},
					HTMLclass: "add_new",
					innerHTML: "<div data-toggle='modal' data-target='#exampleModal' data-whatever='@getbootstrap'><i class='mdi mdi-plus'></i></div>"
				}
			]
		},
		{
			text: {name: "Operation Manager"},
			connectors: {
				style: {
					'arrow-end': 'oval-wide-long'
				}
			},
			stackChildren: true,
			children: [
				{
					text: {name: "Manager I"},
					connectors: {
						style: {
							'arrow-end': 'oval-wide-long'
						}
					},
					stackChildren: true,
					children: [
						{
							text: {name: "Worker I"}
						},
						{
							text: {name: "Worker IV"},
							connectors: {
								style: {
									'arrow-end': 'oval-wide-long'
								}
							},
							stackChildren: true,
							children: [
								{
									text: {name: "Worker II"},
									children: []
								}
							]
						},
						{
							text: {name: "Worker III"}
						}
					]
				},
				{
					text: {name: "Manager II"},
					connectors: {
						style: {
							'arrow-end': 'oval-wide-long'
						}
					},
					stackChildren: true,
					children: [
						{
							text: {name: "Worker I"},
							children: []
						},
						{
							text: {name: "Worker II"},
							children: []
						}
					]
				},
				{
					text: {name: "Manager III"},
					connectors: {
						style: {
							'arrow-end': 'oval-wide-long'
						}
					},
					stackChildren: true,
					children: [
						{
							text: {name: "Worker I"}
						},
						{
							text: {name: "Worker IV"},
							connectors: {
								style: {
									'arrow-end': 'oval-wide-long'
								}
							},
							stackChildren: true,
							children: [
								{
									text: {name: "Worker II"},
									children: []
								},
								{
									text: {name: "Worker III"},
									children: []
								}
							]
						},
						{
							text: {name: "Worker IV"}
						}
					]
				}
			]
		},
		{
			text: {name: "Delivery Manager"},
			stackChildren: true,
			connectors: {
				style: {
					'arrow-end': 'oval-wide-long'
				}
			},
			children: [
				{
					text: {name: "Driver I"},
					children: []
				},
				{
					text: {name: "Driver II"},
					children: []
				},
				{
					text: {name: "Driver III"},
					children: []
				},
				{
					text: {name: "Driver III"},
					children: []
				}
			]
		}
	]
};

export default nodeStructure;