import Paginate from 'vuejs-paginate';
import CRUD from './CRUD';


export default {
	components: {
		Paginate,
	},
	data() {
		return {
			ticketNewList: [],
			ticketNewListPageCount: 0,

			ticketOldList: [],
			ticketOldListPageCount: 0,


			categoryList: [],
			priorityList: [],
			userList: [],
			statusList: [],
		}
	},
	methods: {
		clickPencil(src) {
			this.$router.push({path: src});
		},
		GetTicketsNew(page = 1) {
			CRUD.GetTicketsNew(this.$store.state.user_id, page).then(result => {
				// console.log(result);

				if (result.success) {
					this.ticketNewList = result.clientCards;
					this.ticketNewListPageCount = Math.ceil(result.count / 10);

					this.categoryList = result.categories;
					this.priorityList = result.priorities;
					this.userList = result.users;
					this.statusList = result.statuses;

					this.ticketNewList.forEach((item, index, arr) => {
						if (item.deadline) {
							item.deadlineDiff = moment(item.deadline).diff(moment(), 'days');
						}
						else {
							item.deadlineDiff = null;
						}
					});
				}
				else {
					console.log(result.success);
				}
			});
		},

		GetTicketsOld(page = 1) {
			CRUD.GetTicketsOld(this.$store.state.user_id, page).then(result => {
				// console.log(result);

				if (result.success) {
					this.ticketOldList = result.clientCards;
					this.ticketOldListPageCount = Math.ceil(result.count / 10);

					this.categoryList = result.categories;
					this.priorityList = result.priorities;
					this.userList = result.users;
					this.statusList = result.statuses;

					this.ticketOldList.forEach((item, index, arr) => {
						if (item.deadline) {
							item.deadlineDiff = moment(item.deadline).diff(moment(), 'days');
						}
						else {
							item.deadlineDiff = null;
						}
					});
				}
				else {
					console.log(result.success);
				}
			});
		},
	},
	mounted() {

		this.GetTicketsNew();
		this.GetTicketsOld();
	},
}