export default {
	GetTicketsNew(user_id, page) {
		return new Promise((result, reject) => {
			axios
				.get('ClientCard/executor2ClientCardsIncome/' + user_id + '/' + page)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},

	GetTicketsOld(user_id, page) {
		return new Promise((result, reject) => {
			axios
				.get('ClientCard/executor2ClientCardsOut/' + user_id + '/' + page)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},
}