import CKEditor from '@ckeditor/ckeditor5-vue';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Form from '@/models/Form.js';
import CRUD from './CRUD';


// Import FilePond
import vueFilePond from 'vue-filepond';
// Import styles
import 'filepond/dist/filepond.min.css';

const FilePond = vueFilePond();


export default {
	components: {
		ckeditor: CKEditor.component,
		FilePond,
	},
	data() {
		return {
			editor: ClassicEditor,
			editorConfig: {
				// The configuration of the editor.
			},


			callModel: new Form({
				id: 0,
				phoneNumber: null,
				clientId: null,
				infoData: null,
				categoryId: 0,
				categoryIds: [],
				tags: [],
				notes: '',
				operatorUserId: 0,
				executor1Id: 0,
				executor2Id: 0,
				executor2UserId: 0,
				executorCopyId: 0,
				priorityId: 0,
				deadline: null,
				status: 0,
				dateTime: null,
				cityId: 0,
				regionId: 0,
				streetId: 0,
				houseId: 0,
				flatId: 0,
				applicantName: null,
				applicantAddress: null,
				applicantNumber: null,
				clientCardDivider: null,
			}),
			files: [],


			noteList: [],
			userList: [],
			categoryList: [],
			priorityList: [],
			statusList: [],
			fileList: [],


			cityList: [
				{
					id: 1,
					name: 'Tashkent'
				},
				{
					id: 2,
					name: 'Termez'
				},
				{
					id: 3,
					name: 'Bukhara'
				},
				{
					id: 4,
					name: 'Samarkand'
				},
			],
			districtList: [],
			streetList: [],
			houseList: [],
			flatList: [],
		}
	},
	methods: {
		Save() {
			this.$store.state.loader = true;
			this.callModel.executor2UserId = parseInt(this.$store.state.user_id);

			let formData = new FormData();
			formData.set('clientCardString', JSON.stringify(this.callModel.data()));
			this.files.forEach((item, index, arr) => {
				formData.append('files', item);
			});
			// console.log(this.callModel.data());


			CRUD.Update(formData).then(result => {
				// console.log(result);

				if (result.success) {
					this.$store.state.loader = false;

					this.$router.push('/complaints');
				}
				else {
					console.log(result.message);
				}
			});
		},




		handleFilePond() {
			let temp_arr = [];
			this.$refs.pond.getFiles().forEach((item, index, arr) => {
				temp_arr.push(item.file);
			});
			this.files = temp_arr;

			// console.log(this.$refs.pond.getFiles());
		},

		DownloadFile(url, filename) {
			this.$store.state.loader = true;

			axios
				.get(url, {
					responseType:'blob'
				})
				.then(response => {
					this.$store.state.loader = false;


					const a = document.createElement("a");
					a.style.display = "none";
					document.body.appendChild(a);

					// Set the HREF to a Blob representation of the data to be downloaded
					a.setAttribute("href", window.URL.createObjectURL(new Blob([response.data])));

					// Use download attribute to set set desired file name
					a.setAttribute("download", filename);

					a.click();

					// Cleanup
					document.body.removeChild(a);
				});
		},



		GetPlaces() {
			CRUD.GetDistrict(this.callModel.cityId).then(result => {
				// console.log(result);

				if (result.success) {
					this.districtList = result.districts;
				}
				else {
					console.log(result.message);
				}
			});
			CRUD.GetStreet(this.callModel.regionId).then(result => {
				// console.log(result);

				if (result.success) {
					this.streetList = result.streets;
				}
				else {
					console.log(result.message);
				}
			});
			CRUD.GetHouse(this.callModel.streetId).then(result => {
				// console.log(result);

				if (result.success) {
					this.houseList = result.houses;
				}
				else {
					console.log(result.message);
				}
			});
			CRUD.GetFlat(this.callModel.houseId).then(result => {
				// console.log(result);

				if (result.success) {
					this.flatList = result.flats;
				}
				else {
					console.log(result.message);
				}
			});
		},
	},
	computed: {
		baseUrl() {
			return this.$store.state.baseURL;
		}
	},
	mounted() {
		CRUD.Read(this.$route.params.id).then(result => {
			// console.log(result);

			if (result.success) {
				this.noteList = result.notes;
				this.userList = result.users;
				this.categoryList = result.categories;
				this.priorityList = result.priorities;
				this.statusList = result.statuses;
				this.fileList = result.files;


				result.clientCard.notes = '';
				this.callModel.setData(result.clientCard);


				this.GetPlaces();
			}
			else {
				console.log(result.message);
			}
		});

	},
}