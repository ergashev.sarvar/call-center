export default {
	Read(id) {
		return new Promise((result, reject) => {
			axios
				.get('/ClientCard/clientCard/' + id)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	Update(formData) {
		return new Promise((result, reject) => {
			axios
				.patch('ClientCard/clientCardNew', formData)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},



	GetDistrict(cityId) {
		return new Promise((result, reject) => {
			axios.get('Hokimiyat/District/' + cityId)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetStreet(districtId) {
		return new Promise((result, reject) => {
			axios.get('Hokimiyat/Street/' + districtId)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetHouse(streetId) {
		return new Promise((result, reject) => {
			axios.get('Hokimiyat/House/' + streetId)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetFlat(houseId) {
		return new Promise((result, reject) => {
			axios.get('Hokimiyat/Flat/' + houseId)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},
}