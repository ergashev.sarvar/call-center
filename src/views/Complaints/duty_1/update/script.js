// import VueTags from "vue-tags";
import CKEditor from '@ckeditor/ckeditor5-vue';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Paginate from 'vuejs-paginate';
import CRUD from "./CRUD";
import Form from '@/models/Form.js';


export default {
	components: {
		// VueTags,
		ckeditor: CKEditor.component,
		Paginate,
	},
	data() {
		return {
			editor: ClassicEditor,
			editorConfig: {
				// The configuration of the editor.
			},


			callModel: new Form({
				id: 0,
				phoneNumber: null,
				clientId: null,
				infoData: null,
				categoryId: '',
				categoryIds: [],
				tags: [],
				notes: '',
				operatorUserId: 0,
				executor1Id: 0,
				executor2Id: 0,
				executorCopyId: 0,
				priorityId: 0,
				deadline: null,
				status: 0,
				dateTime: null,
				cityId: 0,
				regionId: 0,
				streetId: 0,
				houseId: 0,
				flatId: 0,
				applicantName: null,
				applicantAddress: null,
				applicantNumber: null,
				clientCardDivider: null,
			}),
			files: [],
			categorySelectedList: [],
			executorSelectedList: [],
			deadlineSelectedList: [],


			noteList: [],
			userList: [],
			categoryList: [],
			priorityList: [],
			statusList: [],
			organizationList: [],
			fileList: [],



			cityList: [
				{
					id: 1,
					name: 'Tashkent'
				},
				{
					id: 2,
					name: 'Termez'
				},
				{
					id: 3,
					name: 'Bukhara'
				},
				{
					id: 4,
					name: 'Samarkand'
				},
			],
			districtList: [],
			streetList: [],
			houseList: [],
			flatList: [],
			communalsList: new Form({
				suvsoz: {
					elements: [],
					success: false,
				},
				teploEnergo: {
					elements: [],
					success: false,
				},
				uzbekEnergo: {
					elements: [],
					success: false,
				},
				uzTransGaz: {
					elements: [],
					success: false,
				},
				maxsusTrans: {
					elements: [],
					success: false,
				},
			}),
			objectInfo: {},


			clientHistoryList: [],
			clientHistoryListPageCount: 0,

			tableArr: [],
	}
	},
	computed: {
		baseUrl() {
			return this.$store.state.baseURL;
		},
		filteredTable() {
			let newArr = [];
			let tableArr = this.tableArr;
			for (let i = 0, arrLength = tableArr.length; i <= arrLength; i++) {
				if (tableArr[i].payment) {
					for (let n = 0; n <= arrLength; n++) {
						if (tableArr[n].meterReadings) {
							newArr.push({date: tableArr[i].date, meterReadings: tableArr[n].meterReadings, payment: tableArr[i].payment})
							break;
						}
					}
				}
				if (newArr.length === 1) {
					break;
				}
			}
			return newArr;
		}
	},
	methods: {
		createTableFunc() {
			console.log(this.communalsList);
			let htmlTable = '';
			let emptyArr = [];
			if (this.communalsList.uzbekEnergo.success) {
				htmlTable = this.communalsList.uzbekEnergo.elements.find(x => x.key === 'vitrinaInfo').value;

				let table = htmlTable.split('</table>')[0];
				table = table.split('<tr>');
				table.splice(0, 3);
				table.forEach((str, index) => {
					str = str.split('>&nbsp;<').join('><');
					// str = str.split('><').join('>&nbsp;<');
					table[index] = str.split('>')
				})
				table.forEach((arr) => {
					arr.forEach((str, index, innerArr) => {
						innerArr[index] = str.split('<')[0]
					})
					arr.splice(0, 1);
				})
				table.forEach((arr) => {
					let newObj = {};
					arr.forEach((str, index) => {
						let name;
						if (index === 0) {
							name = 'date'
						} else if (index === 1) {
							name = 'action'
						} else if (index === 2) {
							name = 'meterReadings'
						} else if (index === 3) {
							name = 'difference'
						} else if (index === 4) {
							name = 'kw'
						} else if (index === 5) {
							name = 'pricing'
						} else if (index === 6) {
							name = 'accrued'
						} else if (index === 7) {
							name = 'payment'
						} else if (index === 8) {
							name = 'debt'
						} else if (index === 9) {
							name = 'prepayment'
						}
						newObj[name] = str
					});
					emptyArr.push(newObj);
				});
			}

			this.tableArr = emptyArr;
		},




		Save() {
			this.callModel.executor1Id = parseInt(this.$store.state.user_id);
			if (this.callModel.status === 1) {
				this.callModel.status = 4;
			}

			this.callModel.priorityId = this.callModel.priorityId === null ? 0 : this.callModel.priorityId.id;
			this.callModel.executorCopyId = this.callModel.executorCopyId === null ? 0 : this.callModel.executorCopyId.id;


			if (this.callModel.categoryIds.length > 1) {
				let clientCardDividerList = [];
				this.categorySelectedList.forEach((item, index, array) => {
					if (item !== null) {
						clientCardDividerList.push({
							categoryId: this.categorySelectedList[index] ? this.categorySelectedList[index].id.toString() : '0',
							executor2Id: this.executorSelectedList[index] ? this.executorSelectedList[index].id : 0,
							deadline: this.deadlineSelectedList[index] ? this.deadlineSelectedList[index].toString() : null,
						});
					}
				});

				this.callModel.clientCardDivider = clientCardDividerList;
			}
			else {
				this.callModel.categoryId = this.callModel.categoryId === null ? '' : this.callModel.categoryId.id.toString();
				this.callModel.categoryIds[0] = this.callModel.categoryId;
				this.callModel.executor2Id = this.callModel.executor2Id === null ? 0 : this.callModel.executor2Id.id;
			}


			// console.log(this.callModel.data());


			let formData = new FormData();
			formData.set('clientCardString', JSON.stringify(this.callModel.data()));
			// this.files.forEach((item, index, arr) => {
			// 	formData.append('files', item);
			// });


			// console.log(this.files);


			CRUD.Update(formData).then(result => {
				// console.log(result);

				if (result.success) {
					this.$router.push('/complaints');
				}
				else {
					console.log(result.message);
				}
			});
		},

		DownloadFile(url, filename) {
			axios
				.get(url, {
					responseType:'blob'
				})
				.then(response => {
					const a = document.createElement("a");
					a.style.display = "none";
					document.body.appendChild(a);

					// Set the HREF to a Blob representation of the data to be downloaded
					a.setAttribute("href", window.URL.createObjectURL(new Blob([response.data])));

					// Use download attribute to set set desired file name
					a.setAttribute("download", filename);

					a.click();

					// Cleanup
					document.body.removeChild(a);
				});
		},



		HandleFilePond() {
			let temp_arr = [];
			this.$refs.pond.getFiles().forEach((item, index, arr) => {
				temp_arr.push(item.file);
			});
			this.files = temp_arr;

			// console.log(this.$refs.pond.getFiles());
		},



		GetPlaces() {
			CRUD.GetDistrict(this.callModel.cityId).then(result => {
				// console.log(result);

				if (result.success) {
					this.districtList = result.districts;
				}
				else {
					console.log(result.message);
				}
			});
			CRUD.GetStreet(this.callModel.regionId).then(result => {
				// console.log(result);

				if (result.success) {
					this.streetList = result.streets;
				}
				else {
					console.log(result.message);
				}
			});
			CRUD.GetHouse(this.callModel.streetId).then(result => {
				// console.log(result);

				if (result.success) {
					this.houseList = result.houses;
				}
				else {
					console.log(result.message);
				}
			});
			CRUD.GetFlat(this.callModel.houseId).then(result => {
				// console.log(result);

				if (result.success) {
					this.flatList = result.flats;
				}
				else {
					console.log(result.message);
				}
			});
		},

		GetHistoryClient(page = 1) {
			CRUD.GetHistoryClient(this.callModel.phoneNumber, page).then(result => {
				// console.log(result);

				this.clientHistoryList = result.callHistoryResponses;
				this.clientHistoryListPageCount = Math.ceil(result.count / 10);

				window.scroll({
					top: 0,
					left: 0,
					behavior: 'smooth'
				});
			});
		},

		AudioPlay(event) {
			let audio = document.querySelectorAll('.call_discuss_rec_play audio');
			for (let i = 0; i < audio.length; i++) {
				if (audio[i] === event.target) continue;
				audio[i].pause();
			}
		},
	},
	mounted() {

		CRUD.Read(this.$route.params.id).then(result => {
			// console.log(result);

			if (result.success) {
				if (result.clientCard.tags.length === 1 && result.clientCard.tags[0] === '') {
					result.clientCard.tags = [];
				}



				result.clientCard.notes = '';
				this.callModel.setData(result.clientCard);
				if (result.clientCard.infoData && JSON.parse(result.clientCard.infoData).hasOwnProperty('objectInfo')) {
					this.objectInfo = JSON.parse(result.clientCard.infoData).objectInfo;
					this.communalsList.setData(JSON.parse(result.clientCard.infoData).communalsList);
					this.createTableFunc();
				}

				this.noteList = result.notes;
				this.userList = result.users;
				this.categoryList = result.categories;
				this.priorityList = result.priorities;
				this.statusList = result.statuses;
				this.organizationList = result.organizations;
				this.fileList = result.files;



				if (this.callModel.categoryIds.length > 1) {
					this.callModel.categoryIds.forEach((item, index, array) => {
						this.categorySelectedList.push({
							id: parseInt(item),
							name: this.categoryList.find(x => x.id === parseInt(item)).name
						});
					});
				}
				else {
					if (this.categoryList.find(x => x.id === parseInt(this.callModel.categoryId))) {
						this.callModel.categoryId = {
							id: parseInt(this.callModel.categoryId),
							name: this.categoryList.find(x => x.id === parseInt(this.callModel.categoryId)).name
						};
					}
					else {
						this.callModel.categoryId = null;
					}

					if (this.organizationList.find(x => x.id === this.callModel.executor2Id)) {
						this.callModel.executor2Id = {
							id: this.callModel.executor2Id,
							name: this.organizationList.find(x => x.id === this.callModel.executor2Id).name
						};
					}
					else {
						this.callModel.executor2Id = null;
					}

					if (this.callModel.deadline && this.callModel.deadline !== '') {
						this.callModel.deadline = moment(this.callModel.deadline).format('YYYY-MM-DD');
					}
				}


				if (this.priorityList.find(x => x.id === this.callModel.priorityId)) {
					this.callModel.priorityId = {
						id: this.callModel.priorityId,
						name: this.priorityList.find(x => x.id === this.callModel.priorityId).name
					};
				}
				else {
					this.callModel.priorityId = null;
				}



				if (this.userList.find(x => x.id === this.callModel.executorCopyId)) {
					this.callModel.executorCopyId = {
						id: this.callModel.executorCopyId,
						fio: this.userList.find(x => x.id === this.callModel.executorCopyId).fio
					};
				}
				else {
					this.callModel.executorCopyId = null;
				}



				this.GetPlaces();
				this.GetHistoryClient();
			}
			else {
				console.log(result.message);
			}
		});
	},
}