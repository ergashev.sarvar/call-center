export default {
	Read(id) {
		return new Promise((result, reject) => {
			axios
				.get('/ClientCard/clientCard/' + id)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	Update(formData) {
		return new Promise((result, reject) => {
			axios
				.patch('ClientCard/clientCardNew', formData)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetHistoryClient(operator_id, page) {
		return new Promise((result, reject) => {
			axios
				.get('ClientCard/callHistoryClient/' + operator_id + '/' + page)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},
}