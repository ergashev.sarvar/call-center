import CKEditor from '@ckeditor/ckeditor5-vue';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Paginate from 'vuejs-paginate';
import Form from '@/models/Form.js';
import CRUD from './CRUD';


// Import FilePond
import vueFilePond from 'vue-filepond';
// Import styles
import 'filepond/dist/filepond.min.css';

const FilePond = vueFilePond();


export default {
	components: {
		ckeditor: CKEditor.component,
		FilePond,
		Paginate,
	},
	data() {
		return {
			editor: ClassicEditor,
			editorConfig: {
				// The configuration of the editor.
			},


			callModel: new Form({
				id: 0,
				phoneNumber: null,
				clientId: null,
				infoData: null,
				categoryId: 0,
				tags: [],
				notes: '',
				operatorUserId: null,
				executor1Id: 0,
				executor2Id: 0,
				executorCopyId: 0,
				priorityId: 0,
				status: 0,
				dateTime: null,
				cityId: 0,
				regionId: 0,
				streetId: 0,
				houseId: 0,
				flatId: 0,
				applicantName: null,
				applicantAddress: null,
				applicantNumber: null,
			}),
			files: [],


			noteList: [],
			userList: [],
			categoryList: [],
			priorityList: [],
			statusList: [],
			fileList: [],


			clientHistoryList: [],
			clientHistoryListPageCount: 0,
		}
	},
	methods: {
		Save() {
			this.callModel.executor1Id = parseInt(this.$store.state.user_id);

			let formData = new FormData();
			formData.set('clientCardString', JSON.stringify(this.callModel.data()));
			this.files.forEach((item, index, arr) => {
				formData.append('files', item);
			});

			// console.log(this.callModel.data());


			CRUD.Update(formData).then(result => {
				// console.log(result);

				if (result.success) {
					this.$router.push('/complaints');
				}
				else {
					console.log(result.message);
				}
			});
		},


		StatusChange(val) {
			this.callModel.status = val;
		},

		HandleFilePond() {
			let temp_arr = [];
			this.$refs.pond.getFiles().forEach((item, index, arr) => {
				temp_arr.push(item.file);
			});
			this.files = temp_arr;

			// console.log(this.$refs.pond.getFiles());
		},

		DownloadFile(url, filename) {
			axios
				.get(url, {
					responseType:'blob'
				})
				.then(response => {
					const a = document.createElement("a");
					a.style.display = "none";
					document.body.appendChild(a);

					// Set the HREF to a Blob representation of the data to be downloaded
					a.setAttribute("href", window.URL.createObjectURL(new Blob([response.data])));

					// Use download attribute to set set desired file name
					a.setAttribute("download", filename);

					a.click();

					// Cleanup
					document.body.removeChild(a);
				});
		},

		GetHistoryClient(page = 1) {
			CRUD.GetHistoryClient(this.callModel.phoneNumber, page).then(result => {
				// console.log(result);

				this.clientHistoryList = result.callHistoryResponses;
				this.clientHistoryListPageCount = Math.ceil(result.count / 10);

				window.scroll({
					top: 0,
					left: 0,
					behavior: 'smooth'
				});
			});
		},

		AudioPlay(event) {
			let audio = document.querySelectorAll('.call_discuss_rec_play audio');
			for (let i = 0; i < audio.length; i++) {
				if (audio[i] === event.target) continue;
				audio[i].pause();
			}
		},
	},
	computed: {
		baseUrl() {
			return this.$store.state.baseURL;
		}
	},
	mounted() {
		CRUD.Read(this.$route.params.id).then(result => {
			// console.log(result);

			if (result.success) {
				result.clientCard.notes = '';
				this.callModel.setData(result.clientCard);
				this.noteList = result.notes;
				this.userList = result.users;
				this.categoryList = result.categories;
				this.priorityList = result.priorities;
				this.statusList = result.statuses;
				this.fileList = result.files;

				this.GetHistoryClient();
			}
			else {
				console.log(result.message);
			}
		});

	},
}