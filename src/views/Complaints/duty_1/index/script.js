import Paginate from 'vuejs-paginate';
import CRUD from './CRUD';


export default {
	components: {
		Paginate,
	},
	data() {
		return {
			ticketNewList: [],
			ticketNewListPageCount: 0,

			ticketOldList: [],
			ticketOldListPageCount: 0,


			userList: [],
			categoryList: [],
			priorityList: [],
			statusList: [],
		}
	},
	methods: {
		clickPencil(src) {
			this.$router.push({path: src});
		},
		GetTicketsNew(page = 1) {
			CRUD.GetTicketsNew(page).then(result => {
				// console.log(result);

				if (result.success) {
					this.userList = result.users;
					this.categoryList = result.categories;
					this.priorityList = result.priorities;
					this.statusList = result.statuses;

					this.ticketNewList = result.clientCards;
					this.ticketNewListPageCount = Math.ceil(result.count / 10);
				}
				else {
					console.log(result.success);
				}
			});
		},

		GetTicketsOld(page = 1) {
			CRUD.GetTicketsOld(page).then(result => {
				// console.log(result);

				if (result.success) {
					this.userList = result.users;
					this.categoryList = result.categories;
					this.priorityList = result.priorities;
					this.statusList = result.statuses;

					this.ticketOldList = result.clientCards;
					this.ticketOldListPageCount = Math.ceil(result.count / 10);

					this.ticketOldList.forEach((item, index, arr) => {
						if (item.deadline) {
							item.deadlineDiff = moment(item.deadline).diff(moment(), 'days');
						}
						else {
							item.deadlineDiff = null;
						}
					});
				}
				else {
					console.log(result.success);
				}
			});
		},
	},
	mounted() {

		this.GetTicketsNew();
		this.GetTicketsOld();
	},
}