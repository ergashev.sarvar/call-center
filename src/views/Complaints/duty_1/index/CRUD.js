export default {
	GetTicketsNew(page) {
		return new Promise((result, reject) => {
			axios
				.get('ClientCard/executor1ClientCardsIncome/' + page)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},

	GetTicketsOld(page) {
		return new Promise((result, reject) => {
			axios
				.get('ClientCard/executor1ClientCardsOut/' + page)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},
}