export default {
	Create(model) {
		return new Promise((result, reject) => {
			axios.post('/TimeSheetAdmin/timesheetGroup', model)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	Read() {

	},


	Update() {

	},


	Delete(model) {
		return new Promise((result, reject) => {
			axios.delete('/TimeSheetAdmin/timesheetGroup',
				{
					"data": {
						"DayOfWeek": model.dayOfWeek,
						"GroupId": model.groupId,
					}
				}
				)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetAll() {
		return new Promise((result, reject) => {
			axios.get('/TimeSheetAdmin/timesheetGroup')
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	ConvertToDB(model) {
		return {
			"dayOfWeek": model.dayOfWeek,
			"groupId": model.groupId,
			"maxParkingTimeLength": model.maxParkingTimeLength,
			"minOnlineOperators": model.minOnlineOperators,
			"maxParkingCount": model.maxParkingCount,
			"starTime": model.start,
			"endTime": model.end,
		};
	},


	ConvertToCalendar(model) {
		model.start = model.starTime;
		model.end = model.endTime;
		model.title = model.group.name;
		model.className = 'calendar_group';

		return model;
	},
}