export default {
	Create(model) {
		return new Promise((result, reject) => {
			axios.post('/TimeSheetAdmin/timesheetMode', model)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	Read() {

	},


	Update() {

	},


	Delete(model) {
		return new Promise((result, reject) => {
			axios.delete('/TimeSheetAdmin/timesheetMode',
				{
					"data": {
						"DayOfWeek": model.dayOfWeek,
						"ModeId": model.modeId,
					}
				}
				)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetAll() {
		return new Promise((result, reject) => {
			axios.get('/TimeSheetAdmin/timesheetMode')
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	ConvertToDB(model) {
		return {
			"dayOfWeek": model.dayOfWeek,
			"modeId": model.modeId,
			"starTime": model.start,
			"endTime": model.end,
		};
	},


	ConvertToCalendar(model) {
		model.start = model.starTime;
		model.end = model.endTime;
		model.title = model.mode.name;
		model.className = 'calendar_regime';

		return model;
	},
}