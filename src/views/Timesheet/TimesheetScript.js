import '@/../public/lib/fullcalendar/fullcalendar.min.js'

import calendarOptions from './calendarOptions';
import GroupEventCRUD from './GroupEventCRUD';
import RegimeEventCRUD from './RegimeEventCRUD';

export default {
	components: {},


	data() {
		return {
			calendarOpt: calendarOptions,
			groupList: [],
			regimeList: [],
			eventType: 1,
			eventGroupModel: {},
			eventRegimeModel: {},
			errorMessage: '',
			eventRead: false,
		}
	},



	methods: {

		SaveGroup() {
			GroupEventCRUD.Create(this.eventGroupModel)
				.then(result => {
					// console.log(result);

					if (result.success) {
						$('#exampleModal').modal('hide');
						this.GetAll();
					}
					else {
						this.errorMessage = result.message;
					}
				});
		},
		DeleteGroup() {
			GroupEventCRUD.Delete(this.eventRead)
				.then(result => {
					// console.log(result);

					if (result.success) {
						$('#exampleModal2').modal('hide');
						this.GetAll();
					}
					else {
						console.log(result.message);
					}
				});
		},




		SaveRegime() {
			RegimeEventCRUD.Create(this.eventRegimeModel)
				.then(result => {
					// console.log(result);

					if (result.success) {
						$('#exampleModal').modal('hide');
						this.GetAll();
					}
					else {
						this.errorMessage = result.message;
					}
				});
		},
		DeleteRegime() {
			RegimeEventCRUD.Delete(this.eventRead)
				.then(result => {
					// console.log(result);

					if (result.success) {
						$('#exampleModal2').modal('hide');
						this.GetAll();

						// $('#calendar').fullCalendar('removeEvents', this.eventRead._id);
					}
					else {
						console.log(result.message);
					}
				});
		},



		GetAll() {
			$('#calendar').fullCalendar('removeEvents');

			GroupEventCRUD.GetAll()
				.then(result => {
					// console.log(result);

					if (result.success) {

						let eventList = [];
						for (let i = 0; i < result.timeSheetGroups.length; i++) {
							eventList.push(GroupEventCRUD.ConvertToCalendar(result.timeSheetGroups[i]));
						}

						$('#calendar').fullCalendar('renderEvents', eventList, true);
					}
					else {
						console.log(result.message);
					}
				});


			RegimeEventCRUD.GetAll()
				.then(result => {
					// console.log(result);

					if (result.success) {

						let eventList = [];
						for (let i = 0; i < result.timeSheetModes.length; i++) {
							eventList.push(RegimeEventCRUD.ConvertToCalendar(result.timeSheetModes[i]));
						}

						$('#calendar').fullCalendar('renderEvents', eventList, true);
					}
					else {
						console.log(result.message);
					}
				});
		},
	},



	mounted() {

		this.GetAll();




		this.calendarOpt.select = (start, end, jsEvent, view) => {
			$('#exampleModal').modal('show');

			this.eventGroupModel.starTime = start.format();
			this.eventGroupModel.endTime = end.format();
			this.eventGroupModel.dayOfWeek = end.weekday();

			this.eventRegimeModel.starTime = start.format();
			this.eventRegimeModel.endTime = end.format();
			this.eventRegimeModel.dayOfWeek = end.weekday();
		};

		this.calendarOpt.eventClick = (event, jsEvent, view) => {
			this.eventRead = event;
			// console.log(event);

			$('#exampleModal2').modal('show');
		};

		$('#calendar').fullCalendar(this.calendarOpt);


		$('#exampleModal').on('hidden.bs.modal', (e) => {
			$('#calendar').fullCalendar('unselect');
			this.errorMessage = '';
			this.eventGroupModel = {};
			this.eventRegimeModel = {};
		});





		axios.get('/TimeSheetAdmin/group')
			.then(response => {
				this.groupList = response.data.groups;
			})
			.catch(error => {
				console.log(error.message);
			});

		axios.get('/TimeSheetAdmin/mode')
			.then(response => {
				this.regimeList = response.data.modes;
			})
			.catch(error => {
				console.log(error.message);
			});
	}
}