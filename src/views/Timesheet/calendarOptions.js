export default  {
	// height: 400, // Small Height
	contentHeight: 900, // Large Height
	allDaySlot: false, // All Day header at the top
	slotEventOverlap: false, // Overlapping Events
	// agendaEventMinHeight : 100, // Min height of Event on click
	// hiddenDays: [ 0, 6 ], // days
	columnHeaderFormat: 'dddd', // Name of Columns
	slotDuration: '00:30:00', // Time Interval
	defaultView: 'agendaWeek', // Calendar Mode
	defaultDate: '2019-02-15', // Static Date
	navLinks: false, // zoom in to inner view when clicked
	editable: true, // resize event
	eventLimit: false, // allow "more" link when too many events
	weekends: true, // Saturday & Sunday days
	nowIndicator: true, // Realtime
	selectable: true, // Add Event in a cell
	selectHelper: true, // Cover selected cells with event
	unselectAuto: true, // Deselect when clicked out of Calendar
	unselectCancel: '#exampleModal', // Deselect Exception when clicked out of Calendar
	firstDay: 1, // Set Monday as starting day
	eventStartEditable: false,
	eventOverlap: function(stillEvent, movingEvent) {
		return movingEvent.className[0] !== stillEvent.className[0];
	},
	eventResizeStop: function( event, jsEvent, ui, view ) {
		console.log(event);
		console.log(jsEvent);
		console.log(ui);
		console.log(view);
	},


	header: {
		left: null,
		center: null,
		right: null
	},
	// views: {
	// 	agendaFourDay: {
	// 		type: 'agenda',
	// 		duration: {days: 4},
	// 		buttonText: '4 day'
	// 	},
	// 	month: { // name of view
	// 		// titleFormat: 'YYYY, MM, DD'
	// 		// other view-specific options here
	// 	},
	// 	basic: {
	// 		// options apply to basicWeek and basicDay views
	// 	},
	// 	agenda: {
	// 		// options apply to agendaWeek and agendaDay views
	// 	},
	// 	week: {
	// 		// options apply to basicWeek and agendaWeek views
	// 	},
	// 	day: {
	// 		// options apply to basicDay and agendaDay views
	// 	}
	// },


	/* eventSources - is Event Grouping */
	// eventSources: [
	//
	// 	// your event source
	// 	{
	// 		events: [ // put the array in the `events` property
	// 			{
	// 				title: 'event1',
	// 				start: '2019-01-9T10:00:00',
	// 			},
	// 			{
	// 				title: 'event2',
	// 				start: '2019-01-10T10:00:00',
	// 				end: '2019-01-10T16:00:00',
	// 			},
	// 			{
	// 				title: 'event3',
	// 				start: '2019-01-17T10:00:00',
	// 			}
	// 		],
	// 		color: 'black',     // an option!
	// 		textColor: 'yellow' // an option!
	// 	}
	//
	// 	// any other event sources...
	//
	// ],
	// events: [
		// areas where "Meeting" must be dropped
		// {
		// 	id: 999,
		// 	title: 'Repeating Eventaaaaaaaaaaaaaaa',
		// 	start: '2019-01-09T16:00:00',
		// 	end: '2019-01-09T19:00:00',
		// 	className: 'aaaaa',
		// 	overlap: true,
		// 	backgroundColor: 'yellow',
		// 	borderColor: 'black',
		// 	textColor: 'blue',
		// },
		// {
		// 	id: 'availableForMeeting',
		// 	start: '2019-01-11T10:00:00',
		// 	end: '2019-01-11T16:00:00',
		// 	rendering: 'background'
		// },
		// {
		// 	id: 'availableForMeeting',
		// 	start: '2019-01-13T10:00:00',
		// 	end: '2019-01-13T16:00:00',
		// 	rendering: 'background'
		// },
	// ],



	// select: function (start, end, jsEvent, view) {
	// 	$('#exampleModal').modal('show');
	// 	console.log('select', start.format(), end.format());
	// 	let title = prompt('Event Title:');
	// 	let eventData;
	// 	if (title) {
	// 		eventData = {
	// 			title: title,
	// 			start: start,
	// 			end: end,
	// 			overlap: true,
	// 		};
	// 		$('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
	// 	}
	// 	$('#calendar').fullCalendar('unselect');
	// },

	// dayClick: function (date, jsEvent, view) {

		// alert('Clicked on: ' + date.format());
		//
		// alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
		//
		// alert('Current view: ' + view.name);

		// change the day's background color just for fun
		// $(this).css('background-color', 'red');

	// },
	// eventClick: function(event, jsEvent, view) {
	//
	// 	console.log(event);
	// 	console.log(jsEvent);
	// 	console.log(view);
	//
	// 	event.title = "CLICKED!";
	//
	// 	opens events in a popup window
	// 	alert('eventClick');
	// 	$('#calendar').fullCalendar('unselect');
	// 	return false;
	//
	// 	$('#calendar').fullCalendar('updateEvent', event);
	//
	// }
}