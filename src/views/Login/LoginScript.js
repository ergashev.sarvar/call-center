import Form from '../../models/Form';

export default {
	components: {},
	data() {
		return {
			form: new Form({
				username: '',
				password: '',
				client_id: "123",
			}),
			saveCredentials: !!(this.$store.state.username && this.$store.state.password),
			noUserMessage: null,
			otpSent: false,
		}
	},
	mounted() {
	},
	methods: {
		submitForm() {
			this.form.submit('post', '/login/auth2')
				.then(res => {

					if (res.success === false) {
						this.form.errors.errorMessage = res.message;
					}
					else {
						localStorage.setItem('token', res.data.access_token);
						axios.defaults.headers.common.Authorization = 'Bearer ' + res.data.access_token;
						localStorage.setItem('token_time', res.data.expires_in);
						localStorage.setItem('refresh_token', res.data.refresh_token);
						localStorage.setItem('organization_id', res.data.organization_id);
						localStorage.setItem('permissions', JSON.stringify(res.data.permissions));
						localStorage.setItem('localization', JSON.stringify(res.data.localization));
						localStorage.setItem('operator_id', res.data.operator_id);
						this.$store.state.operator_id = res.data.operator_id + '';
						localStorage.setItem('user_id', res.data.user_id);
						this.$store.state.user_id = res.data.user_id;




						this.$router.push('/dashboards');
					}
				})
				.catch(err => {
					console.log(err);
				})
		},
		SendOTP() {
			axios.get('Admin/otp/' + this.form.username)
				.then(response => {
					if (response.data.success) {
						this.otpSent = true;
						this.noUserMessage = null;
					}
					else {
						this.noUserMessage = response.data.message;
						this.otpSent = false;
					}
				})
				.catch(error => {
					console.log(error.message);
				});
		},

		submitFormOld() {
			this.form.submit('post', '/login/auth')
				.then(res => {

					if (res.success === false) {
						this.form.errors.errorMessage = res.message;
					}
					else {
						localStorage.setItem('token', res.data.access_token);
						axios.defaults.headers.common.Authorization = 'Bearer ' + res.data.access_token;
						localStorage.setItem('token_time', res.data.expires_in);
						localStorage.setItem('refresh_token', res.data.refresh_token);
                        localStorage.setItem('organization_id', res.data.organization_id);
						localStorage.setItem('permissions', JSON.stringify(res.data.permissions));
						localStorage.setItem('localization', JSON.stringify(res.data.localization));
						localStorage.setItem('operator_id', res.data.operator_id);
						this.$store.state.operator_id = res.data.operator_id + '';
						localStorage.setItem('user_id', res.data.user_id);
						this.$store.state.user_id = res.data.user_id;

						if (this.saveCredentials) {
							localStorage.setItem('username', btoa(this.form.username));
							this.$store.state.username = this.form.username;
							localStorage.setItem('password', btoa(this.form.password));
							this.$store.state.password = this.form.password;
						}
						else {
							localStorage.removeItem('username');
							this.$store.state.username = null;
							localStorage.removeItem('password');
							this.$store.state.password = null;
						}



						this.$router.push('/dashboards');
					}
				})
				.catch(err => {
					console.log(err);
				})
		},
	},
}