export default {
	data() {
		return {
			request: {
				name: "",
				textUz: "",
				textRu: "",
				textEn: "",
				orgId: null,
			},
			modelList: [],
			actionType: null,
			id: null,

		}
	},
	methods: {
		getList() {
			this.$http.get('Playmobile/Get').then(res => {
				this.modelList = res.data.introductions;
			})
		},
		deleteGreeting(id) {
			if (confirm("Do you actually delete this item?")) {
				this.$http.delete('Playmobile/Introduction/' + id).then(res => {
					this.$store.state.notifyDelete();
					this.getList();
				})
			}
		},
		readGreeting(id) {
			this.id = id;
			this.actionType = 2;
			this.$http.get('Playmobile/GetIntroduction/' + id).then(res => {
				this.request = res.data.introduction
			})
		},
		addGreeting() {
			this.request.orgId = parseInt(localStorage.getItem("organization_id"));
			this.$http.post('Playmobile/Introduction', this.request).then(res => {
				if (res && res.data.success) {
					$('#modalInfo').modal('hide');
					this.getList();
				}
			})
		},
		updateGreeting() {
			this.request.id = this.id;
			this.$http.put('Playmobile/EditIntroduction', this.request).then(res => {
				if (res && res.data.success) {
					$('#modalInfo').modal('hide');
					this.getList();
				}
			})
		},
		resetForm() {
			this.request.name = '';
			this.request.textEn = '';
			this.request.textRu = '';
			this.request.textUz = '';
			delete this.request.id;
		}

	},
	created() {
		this.getList();
	}
}