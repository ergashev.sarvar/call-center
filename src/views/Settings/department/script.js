export default {
	name: "index",
	data() {
		return {
			modelList: [],
			actionType: null,
			id: null,
			request: {
				name: '',
				orgId: null,
			}
		}
	},
	methods: {
		getList() {
			this.$http.get('Admin/department').then(res => {
				this.modelList = res.data.data;
			})
		},
		readDeal(id) {
			this.id = id;
			this.actionType = 2;
			this.$http.get('Admin/departmentById', {params: {id: id}}).then(res => {
				this.request = res.data.data
			})
		},
		addDeal() {
			this.request.orgId = parseInt(localStorage.getItem("organization_id"));
			this.$http.post('Admin/createDepartment', this.request).then(res => {
				if (res && res.data.success) {
					$('#modalInfo').modal('hide');
					this.getList();
				}
			})
		},
		updateDeal() {
			this.request.id = this.id;
			this.$http.put('Admin/updateDepartment', this.request).then(res => {
				if (res && res.data.success) {
					$('#modalInfo').modal('hide');
					this.getList();
				}
			})
		},
		deleteDeal(id) {
			if (confirm("Do you actually delete this item?")) {
				this.$http.delete('Admin/deleteDepartment', {params: {id: id}}).then(res => {
					this.$store.state.notifyDelete();
					this.getList();
				})
			}
		},
		resetForm() {
			this.request.name = '';
			delete this.request.id;
		}
	},
	created() {
		this.getList();
	}
}