export default {
	name: "index",
	data() {
		return {
			request: {
				name: "",
				phone: "",
				address: "",
				code: null
			},
			modelList: [],
			actionType: null,
			id: null,
		}
	},
	methods: {
		getList() {
			this.$http.get('Admin/organization').then(res => {
				this.modelList = res.data.data;
			})
		},
		addOrg() {
			this.$http.post('Admin/organization', this.request).then(res => {
				if (res && res.data.success) {
					$('#modalInfo').modal('hide');
					this.getList();
				}
			})
		},
		updateOrg() {
			this.request.id = this.id;
			this.$http.put('Admin/organization', this.request).then(res => {
				if (res && res.data.success) {
					$('#modalInfo').modal('hide');
					this.getList();
				}
			})
		},
		readOrg(id) {
			this.id = id;
			this.actionType = 2;
			this.$http.get('Admin/organization/' + id).then(res => {
				this.request = res.data.data
			})
		},
		deleteOrg(id) {
			if (confirm("Do you actually delete this item?")) {
				this.$http.delete('Admin/organization/' + id).then(res => {
					this.$store.state.notifyDelete();
					this.getList();
				})
			}
		}
	},
	created() {
		this.getList();
	}
}