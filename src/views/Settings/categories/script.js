import CRUD from './CRUD'

export default {

	data() {
		return {
			actionType: 0,
			modelList: {},
			model: {},
			parentList: {}
		}
	},


	methods: {
		Create() {
			CRUD.Create(this.model)
				.then(result => {
					// console.log(result);

					if (result.success) {
						$('#modalInfo').modal('hide');
						this.GetAll();
					}
					else {
						console.log(result.message);
					}
				});
		},
		Read(id) {
			CRUD.Read(id)
				.then(result => {
					// console.log(result);

					if (result.success) {
						this.model = result.data;
						$('#modalInfo').modal('show');
					}
					else {
						console.log(result.message);
					}
				});
		},
		Update() {
			CRUD.Update(this.model)
				.then(result => {
					// console.log(result);

					if (result.success) {
						$('#modalInfo').modal('hide');
						this.GetAll();
					}
					else {
						console.log(result.message);
					}
				});
		},
		Delete() {
			CRUD.Delete(this.model.id)
				.then(result => {
					// console.log(result);

					if (result.success) {
						$('#modalInfo').modal('hide');
						this.GetAll();
					}
					else {
						console.log(result.message);
					}
				});
		},
		GetAll() {
			CRUD.GetAll()
				.then(result => {
					// console.log(result);

					if (result.success) {
						this.modelList = result.data;
						this.parentList = result.data;
					}
					else {
						console.log(result.message);
					}
				});
		},
	},


	mounted() {

		this.GetAll();


		$('#modalInfo').on('hidden.bs.modal', (e) => {
			this.model = {};
		});

	},
}
