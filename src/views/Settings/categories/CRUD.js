export default {
	Create(model) {
		return new Promise((result, reject) => {
			axios.post('/Admin/category', model)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	Read(id) {
		return new Promise((result, reject) => {
			axios.get('/Admin/category/' + id)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	Update(model) {
		return new Promise((result, reject) => {
			axios.put('/Admin/category', model)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	Delete(id) {
		return new Promise((result, reject) => {
			axios.delete('/Admin/category/' + id)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetAll() {
		return new Promise((result, reject) => {
			axios.get('/Admin/category')
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},
}