import CRUD from './CRUD'

export default {
	data() {
		return {
			actionType: 0,
			modelList: {},
			model: {},
		}
	},


	methods: {
		Create() {
			if (!this.model.name || this.model.name.trim() === '') {
				return;
			}
			CRUD.Create(this.model)
				.then(result => {
					// console.log(result);

					if (result.success) {
						$('#modalInfo').modal('hide');
						this.GetAll();
					}
					else {
						console.log(result.success);
					}
				});
		},
		Read(id) {
			CRUD.Read(id)
				.then(result => {
					// console.log(result);

					if (result.success) {
						this.model = result.data;
						$('#modalInfo').modal('show');
					}
					else {
						console.log(result.success);
					}
				});
		},
		Update() {
			if (!this.model.name || this.model.name.trim() === '') {
				return;
			}
			CRUD.Update(this.model)
				.then(result => {
					// console.log(result);

					if (result.success) {
						$('#modalInfo').modal('hide');
						this.GetAll();
					}
					else {
						console.log(result.success);
					}
				});
		},
		Delete() {
			CRUD.Delete(this.model.id)
				.then(result => {
					// console.log(result);

					if (result.success) {
						$('#modalInfo').modal('hide');
						this.GetAll();
					}
					else {
						console.log(result.success);
					}
				});
		},
		GetAll() {
			CRUD.GetAll()
				.then(result => {
					// console.log(result);

					if (result.success) {
						this.modelList = result.data;
					}
					else {
						console.log(result.success);
					}
				});
		},
		onSubmit(event) {
			event.preventDefault()
		}
	},


	mounted() {

		this.GetAll();


		$('#modalInfo').on('hidden.bs.modal', (e) => {
			this.model = {};
		});

	},
}