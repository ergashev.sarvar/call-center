export default {
	Create(model) {
		return new Promise((result, reject) => {
			axios.post('/Admin/priority', model)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	Read(id) {
		return new Promise((result, reject) => {
			axios.get('/Admin/priority/' + id)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	Update(model) {
		return new Promise((result, reject) => {
			axios.put('/Admin/priority', model)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	Delete(id) {
		return new Promise((result, reject) => {
			axios.delete('/Admin/priority/' + id)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetAll() {
		return new Promise((result, reject) => {
			axios.get('/Admin/priority')
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},
}