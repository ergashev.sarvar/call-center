
export default {

	data() {
		return {
			serverId: '',
			activationKey: '',
			maxOperators: '',

			reactivationKey: '',

			status: 2,
		}
	},


	methods: {
		Reactivate() {
			axios.post('/Login/activate', {
					"key": this.reactivationKey
				})
				.then(response => {
					if (response.data.success) {
						this.status = 1;
						this.GetAll();
					}
					else {
						this.status = 0;
					}
				})
				.catch(error => {
					console.log(error.message);
				});
		},
		GetAll() {
			axios.get('/Login/ActivationInfo')
				.then(response => {
					this.serverId = response.data.data.serverCode;
					this.activationKey = response.data.data.activationKey;
					this.maxOperators = response.data.data.manOnlineUser;
				})
				.catch(error => {
					console.log(error.message);
				});
		},
	},


	mounted() {


		this.GetAll();



		$('#modalInfo').on('hidden.bs.modal', (e) => {
			this.reactivationKey = '';
		});

	},
}