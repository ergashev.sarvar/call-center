export default {
	Create(model) {
		return new Promise((result, reject) => {
			axios.post('/TimeSheetAdmin/mode', model)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	Read(id) {
		return new Promise((result, reject) => {
			axios.get('/TimeSheetAdmin/mode/' + id)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	Update(model) {
		return new Promise((result, reject) => {
			axios.patch('/TimeSheetAdmin/mode', model)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	Delete(id) {
		return new Promise((result, reject) => {
			axios.delete('/TimeSheetAdmin/mode/' + id)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetAll() {
		return new Promise((result, reject) => {
			axios.get('/TimeSheetAdmin/mode')
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	ConvertToDB(model) {

		model.minOnlineOperators = parseInt(model.minOnlineOperators);
		model.maxParkingCount = parseInt(model.maxParkingCount);

		let h = model.maxParkingTimeLength.HH;
		let m = model.maxParkingTimeLength.mm;
		let s = model.maxParkingTimeLength.ss;

		model.maxParkingTimeLength = parseInt(s);
		model.maxParkingTimeLength += parseInt(m) * 60;
		model.maxParkingTimeLength += parseInt(h) * 3600;

		return model;
	},



	ConvertToModel(model) {

		let h = Math.floor(model.maxParkingTimeLength / 3600);
		model.maxParkingTimeLength %= 3600;
		let m = Math.floor(model.maxParkingTimeLength / 60);
		model.maxParkingTimeLength %= 60;
		let s = model.maxParkingTimeLength;

		model.maxParkingTimeLength = {
			'HH': String(h),
			'mm': String(m),
			'ss': String(s),
		};

		return model;
	},
}