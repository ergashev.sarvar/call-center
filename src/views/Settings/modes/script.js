import VueTimepicker from 'vue2-timepicker'
import RoleCRUD from './ModeCRUD'

export default {
	components: {
		VueTimepicker
	},


	data() {
		return {
			modeActionType: 0,
			modeList: {},
			modeModel: {},
			timePickerValue: {
				'HH': '00',
				'mm': '00',
				'ss': '00',
			},
		}
	},


	methods: {
		ModeCreate() {
			RoleCRUD.Create(RoleCRUD.ConvertToDB(this.modeModel))
				.then(result => {
					// console.log(result);

					if (result.success) {
						$('#modeModal').modal('hide');
						this.ModeGetAll();
					}
					else {
						console.log(result.success);
					}
				});
		},
		ModeRead(id) {
			RoleCRUD.Read(id)
				.then(result => {
					// console.log(result);

					if (result.success) {
						this.modeModel = RoleCRUD.ConvertToModel(result.mode);
						this.timePickerValue = this.modeModel.maxParkingTimeLength;
						$('#modeModal').modal('show');
					}
					else {
						console.log(result.success);
					}
				});
		},
		ModeUpdate() {
			RoleCRUD.Update(RoleCRUD.ConvertToDB(this.modeModel))
				.then(result => {
					// console.log(result);

					if (result.success) {
						$('#modeModal').modal('hide');
						this.ModeGetAll();
					}
					else {
						console.log(result.success);
					}
				});
		},
		ModeDelete() {
			RoleCRUD.Delete(this.modeModel.id)
				.then(result => {
					// console.log(result);

					if (result.success) {
						$('#modeModal').modal('hide');
						this.ModeGetAll();
					}
					else {
						console.log(result.success);
					}
				});
		},
		ModeGetAll() {
			RoleCRUD.GetAll()
				.then(result => {
					// console.log(result);

					this.modeList = result.modes;
				});
		},
	},


	mounted() {

		this.ModeGetAll();


		$('#modeModal').on('hidden.bs.modal', (e) => {
			this.modeModel = {};
			this.timePickerValue = {
				'HH': '00',
				'mm': '00',
				'ss': '00',
			};
		});

	},
	watch: {
		timePickerValue(val) {
			this.modeModel.maxParkingTimeLength = val;
		},
	},
}