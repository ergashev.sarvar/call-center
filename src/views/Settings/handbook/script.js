import Form from '@/models/Form.js';


export default {
	data() {
		return {
			actionType: 0,

			modelList: null,
			model: new Form({
				'id': 0,
				'phoneNumber': null,
				'info': null,
				'orgId': 0,
			}),
			orgList: [],

		}
	},


	methods: {
		CreateAndUpdate() {
			console.log(this.model.data());

			if (this.actionType === 1) {
				this.model.submit('post', '/ShortNumber/addShortNumber').then(result => {
					if (result.success) {
						$('#modalInfo').modal('hide');
						this.$store.state.notifyAdd();

						this.GetAll();
					}
					else {
						console.log(result.message);
					}
				});
			}
			else if (this.actionType === 2) {
				this.model.submit('patch', '/ShortNumber/updateShortNumber').then(result => {
					if (result.success) {
						$('#modalInfo').modal('hide');
						this.$store.state.notifyEdit();

						this.GetAll();
					}
					else {
						console.log(result.message);
					}
				});
			}
		},
		Delete(id) {
			if (confirm('You are about to delete the record! \nAre you sure?')) {
				axios
					.delete('/ShortNumber/deleteShortNumber', {
						params: {
							'id': id
						}
					})
					.then(response => {
						if (response.data.success) {
							this.$store.state.notifyDelete();

							this.GetAll();
						}
						else {
							console.log(response.data.message);
						}
					})
					.catch(error => console.warn(error.message));
			}
		},
		GetAll() {
			axios
				.get('/ShortNumber/getShortNumbers')
				.then(response => {
					this.modelList = response.data.shortNumbers;
				})
				.catch(error => console.warn(error.message));
		},
	},


	async mounted() {
		await axios
			.get('/Admin/organization')
			.then(response => {
				this.orgList = response.data.data;
			})
			.catch(error => console.warn(error.message));


		this.GetAll();

		$('#modalInfo').on('hidden.bs.modal', (e) => {
			this.model.reset();
		});
	},
}