export default {
	data() {
		return {
			modelList: [],
			actionType: null,
			id: null,
			request: {
				name: '',
				orgId: null
			},
			faqList: [],
			isFaq: false,
			faqRequest: {
				theme: '',
				question: '',
				answer: '',
				categoryId: null
			},
			isShown: false,
			faqId: null

		}
	},
	methods: {
		getList() {
			this.$http.get('Playmobile/GetFaqCategories').then(res => {
				this.modelList = res.data.categories
			})
		},
		addCategory() {
			this.$http.post('Playmobile/AddFaqCategory', {}, {params: {name: this.request.name}}).then(res => {
				if (res && res.data.success) {
					$('#modalInfo').modal('hide');
					this.getList();
				}
			})
		},
		updateCategory() {
			this.$http.put('Playmobile/EditFaqCategory', {}, {params: {id: this.id, name: this.request.name}}).then(res => {
				if (res && res.data.success) {
					$('#modalInfo').modal('hide');
					this.getList();
				}
			})
		},
		readCategory(id) {
			this.id = id;
			this.actionType = 2;
			this.$http.get('Playmobile/GetFaqCategory', {params: {id: id}}).then(res => {
				this.request = res.data.faqCategory
			})
		},
		deleteCategroy(id) {
			if (confirm("Do you actually delete this item?")) {
				this.$http.delete('Playmobile/RemoveFaqCategory', {params: {id: id}}).then(res => {
					this.$store.state.notifyDelete();
					this.getList();
				})
			}
		},
		readFaq(id) {
			this.faqId = id;
			this.actionType = 2;
			this.$http.get('Playmobile/GetFaq/id', {params: {id: id}}).then(res => {
				this.faqRequest = res.data.faq
			})
		},
		deleteFaq(id) {
			if (confirm("Do you actually delete this item?")) {
				this.$http.delete('Playmobile/RemoveFaq/id', {params: {id: id}}).then(res => {
					this.$store.state.notifyDelete();
					this.getByCategory(this.id);
				})
			}
		},
		addFaq() {
			this.faqRequest.categoryId = this.id;
			this.$http.post('Playmobile/AddFaq', this.faqRequest).then(res => {
				if (res && res.data.success) {
					$('#modalInfo').modal('hide');
					this.getByCategory(this.id);
				}
			})
		},
		updateFaq() {
			this.faqRequest.id = this.faqId;
			this.$http.put('Playmobile/EditFaq', this.faqRequest).then(res => {
				if (res && res.data.success) {
					$('#modalInfo').modal('hide');
					this.getByCategory(this.id);
				}
			})
		},
		getByCategory(id) {
			this.isShown = true;
			this.id = id;
			this.$http.get('Playmobile/GetFaqs/categoryId', {params: {categoryId: this.id}}).then(res => {
				this.faqList = res.data.faqs;
			})
		},
		resetForm() {
			this.request.name = '';
			this.faqRequest.theme = '';
			this.faqRequest.question = '';
			this.faqRequest.answer = '';
			delete this.faqRequest.id
		}
	},
	created() {
		this.getList();
	}
}