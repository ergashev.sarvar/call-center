import Form from '../../../models/Form';
import Paginate from "vuejs-paginate";

export default {
	data() {
		return {
			search: '',
			pageCount: 0,
			users: [],
			districtList: [],
			id: null,
			userForm: new Form({
				fio: '',
				userName: '',
				operatorId: '',
				phone: '',
				email: '',
				active: '',
				password: '',
				organizationId: null,
				districtId: null,
				id: null
			}),
			moduleForm: new Form({
				userId: '',
				moduleId: '',
				actionId: '',
			}),
			modules: [],
			allModules: [],
			user: null,
			organizationList: []
		}
	},
	components: {
		Paginate,
	},
	methods: {
		clickSearch() {
			this.getSearchedResult()
		},
		getSearchedResult(page = 0) {
			if (page > 0) {
				page = page - 1;
			}
			axios.get('/admin/users/' + page + '?expr=' + this.search )
				.then(response => {
					this.pageCount = response.data.data.pages
					this.users = response.data.data.users
				})
		},
		saveUser() {
			if (this.userForm.id > 0) {
				this.updateUser();
			} else {
				this.createUser();
			}
		},
		createUser() {
			delete this.userForm.id;
			if (!this.userForm.districtId) {
				this.userForm.districtId = null
			}
			this.userForm.submit('post', '/admin/user')
			  .then(response => {
				  if (response.success === true) {
					  $('#userModal').modal('hide');
					  this.$store.state.notifyAdd();
					  this.getUsers();
					  this.userForm.reset();
				  } else {
					  this.$store.state.notify(response.message, 'error');
				  }
			  });
		},
		updateUser() {
			this.userForm.submit('put', '/admin/user')
			  .then(response => {
				  if (response.success === true) {
					  $('#userModal').modal('hide');
					  this.$store.state.notifyEdit();

					  this.getUsers();
					  this.userForm.reset();
				  } else {
					  this.$store.state.notify(response.message, 'error');
				  }
			  });
		},
		deleteUser(user) {
			if (confirm('Are you sure want to delete?')) {
				let indx = this.users.indexOf(user);
				if (indx >= 0) {
					axios.delete('/admin/user/' + user.id)
					  .then(res => {
						  res = res.data;
						  if (res.success === true) {
							  this.users.splice(indx, 1);
							  this.$store.state.notifyDelete();
						  }
					  });
				}
			}
		},
		getUser(user) {
			this.id = user.id;
			this.user = user;
			axios.get('/admin/user/modules/' + user.id)
			  .then(res => {
				  this.modules = res.data.data;
				  this.moduleForm.userId = user.id;
			  })
		},
		deleteModule(module) {
			if (confirm('Are you sure want to delete?')) {
				let indx = this.modules.indexOf(module);
				if (indx >= 0) {
					axios.delete('/admin/user/module/' + module.id)
					  .then(res => {
						  if (res.data.success === true) {
							  this.modules.splice(indx, 1);
							  this.$store.state.notifyDelete();
						  }
					  })
				}
			}
		},
		saveModule() {
			this.moduleForm.submit('post', '/admin/user/module')
			  .then(res => {
				  if (res.success === true) {
					  let module = this.allModules.find(item => item.id === this.moduleForm.moduleId);
					  let action = '';
					  switch (this.allModules.actionId) {
						  case 0: action = 'None'; break;
						  case 1: action = 'View'; break;
						  default: action = 'Edit';
					  }
					  this.$store.state.notifyAdd();
					  this.modules.push({
						  moduleName: module.name,
						  actionName: action,
						  id: res.id
					  });
					  $('#moduleModal').modal('hide');
				  } else {
					  $('#moduleModal').modal('hide');
					  $('#alreadyExists').modal('show');
				  }
			  })
		},
		getModules() {
			axios.get('/admin/modules')
			  .then(res => {
				  this.allModules = res.data.data;
			  })
		},
		cleanForm() {
			this.userForm = new Form({
				fio: '',
				userName: '',
				phone: '',
				email: '',
				active: '',
				password: '',
				operatorId: '',
				id: null
			})
		},
		getDistrictList() {
			axios.get('/Hokimiyat/District/100000')
				.then(res => {
					this.districtList = res.data.districts
				})
		},
		getOrganizationList() {
			axios.get('/Admin/organization')
				.then(res => {
					this.organizationList = res.data.data
				})
		}
	},
	created() {
		this.clickSearch();
		this.getModules();
		this.getDistrictList();
		this.getOrganizationList();
	}
}
