export default {
	data() {
		return {
			allQueues: [],
			allAgents: [],
			allTiers: [],


			queueFormConstructor: {
				name: "",
				strategy: "",
				mohSound: "",
				recordTemplate: "",
				timeBaseScore: "",
				tierRulesApply: "",
				tierRuleWaitSecond: "",
				tierRuleWaitMultiplyLevel: "",
				tierRuleNoAgentNoWait: "",
				discardAbandonedAfter: "",
				abandonedResumeAllowed: "",
				maxWaitTime: "",
				maxWaitTimeWithNoAgent: "",
			},
			queueForm: {...this.queueFormConstructor},
			queueType: 0,


			agentFormConstructor: {
				name: '',
				type: '',
				contact: '',
				status: '',
				maxNoAnswer: '',
				wrapUpTime: '',
				rejectDelayTime: '',
				busyDelayTime: '',
			},
			agentForm: {...this.agentFormConstructor},
			agentType: 0,


			tierFormConstructor: {
				agent: '',
				queue: '',
				level: '',
				position: '',
			},
			tierForm: {...this.tierFormConstructor},
			tierType: 0
		}
	},
	methods: {
		clearQueueForm() {
			this.queueType = 1;
			this.queueForm = {...this.queueFormConstructor}
		},
		queueSendFormType(obj) {
			this.queueType = 2;
			this.queueForm = {...obj};
			$('#add_queues').modal('show');
		},
		queueSendForm() {
			if (this.queueType === 1) {
				axios.post('/callcenter/queue', this.queueForm)
					.then(response => {
						this.getAllQueues();
						$('#add_queues').modal('hide');
						this.$store.state.notifyAdd();
					}).catch(error => console.log(error));
			}
			else {
				axios.patch('/callcenter/queue', this.queueForm)
					.then(response => {
						this.getAllQueues();
						$('#add_queues').modal('hide');
						this.$store.state.notifyEdit();
					}).catch(error => console.log(error));
			}
		},
		deleteQueue(id) {
			if (confirm('Are you sure to delete?')) {
				axios.delete('/callcenter/queue/' + id)
					.then(response => {
						this.getAllQueues();
						this.$store.state.notifyDelete();
					}).catch(error => console.log(error));
			}
		},
		getAllQueues() {
			axios.get('/callcenter/queues')
				.then(response => {
					this.allQueues = response.data.queues;
				}).catch(error => console.log(error));
		},


		clearAgentForm() {
			this.agentType = 1;
			this.agentForm = {...this.agentFormConstructor}
		},
		agentSendFormType(obj) {
			this.agentType = 2;
			this.agentForm = {...obj};
			$('#s_c_c_add_agents').modal('show');
		},
		agentSendForm() {
			if (this.agentType === 1) {
				axios.post('/callcenter/agent', this.agentForm)
					.then(response => {
						this.getAllAgents();
						$('#s_c_c_add_agents').modal('hide');
						this.$store.state.notifyAdd();
					}).catch(error => console.log(error));
			}
			else {
				axios.patch('/callcenter/agent', this.agentForm)
					.then(response => {
						this.getAllAgents();
						$('#s_c_c_add_agents').modal('hide');
						this.$store.state.notifyEdit();
					}).catch(error => console.log(error));
			}
		},
		deleteAgent(id) {
			if (confirm('Are you sure to delete?')) {
				axios.delete('/callcenter/agent/' + id)
					.then(response => {
						this.getAllAgents();
						this.$store.state.notifyDelete();
					}).catch(error => console.log(error));
			}
		},
		getAllAgents() {
			axios.get('/CallCenter/agents')
				.then(response => {
					this.allAgents = response.data.agents;
				}).catch(error => console.log(error));
		},


		clearTierForm() {
			this.tierType = 1;
			this.tierForm = {...this.tierFormConstructor};
		},
		tierSendFormType(obj) {
			this.tierType = 2;
			this.tierForm = {...obj};
			$('#s_c_c_add_tier').modal('show');
		},
		tierSendForm() {
			if (this.tierType === 1) {
				axios.post('/callcenter/tier', this.tierForm)
					.then(response => {
						this.getAllTiers();
						$('#s_c_c_add_tier').modal('hide');
						this.$store.state.notifyAdd();
					}).catch(error => console.log(error));
			}
			else {
				axios.patch('/callcenter/tier', this.tierForm)
					.then(response => {
						this.getAllTiers();
						$('#s_c_c_add_tier').modal('hide');
						this.$store.state.notifyEdit();
					}).catch(error => console.log(error));
			}
		},
		deleteTier(id) {
			if (confirm('Are you sure to delete?')) {
				axios.delete('/callcenter/tier/' + id)
					.then(response => {
						this.getAllTiers();
						this.$store.state.notifyDelete();
					}).catch(error => console.log(error));
			}
		},
		getAllTiers() {
			axios.get('/CallCenter/tiers')
				.then(response => {
					this.allTiers = response.data.tiers;
				}).catch(error => console.log(error));
		},


		getXml() {
			axios.get('/CallCenter/getXml')
				.then(response => {
					if (response.data.success) {
						this.$store.state.notifySave();
					}
				}).catch(error => console.log(error));
		},
	},
	created() {
		this.getAllQueues();
		this.getAllAgents();
		this.getAllTiers();
	}
}