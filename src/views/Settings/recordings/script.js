import Datepicker from "vue-bootstrap-datetimepicker";
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';
import Paginate from 'vuejs-paginate';


export default {
	components: {
		Datepicker,
		Paginate
	},
	data() {
		return {
			agentNumber: null,
			callerNumber: null,
			fromDate: null,
			toDate: null,
			options: {
				format: 'YYYY-MM-DD HH:m:s',
				useCurrent: false,
				showClear: true,
				showClose: true,
				locale: 'ru',
				icons: {
					time: 'mdi mdi-clock-outline',
					date: 'mdi mdi-calendar',
					up: 'mdi mdi-arrow-up',
					down: 'mdi mdi-arrow-down',
					previous: 'mdi mdi-arrow-left',
					next: 'mdi mdi-arrow-right',
					today: 'mdi mdi-calendar-today',
					clear: 'mdi mdi-delete-outline',
					close: 'mdi mdi-alpha-x-circle-outline'
				}
			},

			recordListData: {
				result: [],
				pages: 0,
			},
			paginationKey: 0,

		}
	},

	mounted() {

		this.GetAll();
	},

	methods: {
		GetAll(page = 1) {
			if (page === 1) this.paginationKey++;

			let data = {
				'agentNumber': this.agentNumber === '' ? null : this.agentNumber,
				'callerNumber': this.callerNumber === '' ? null : this.callerNumber,
				'from': this.fromDate ? moment(this.fromDate).format() : null,
				'to': this.toDate ? moment(this.toDate).format() : null,
				'page': page,
			};

			axios
				.post('/Cdr/CallRecordHistory', data)
				.then(response => {
					this.recordListData = response.data;
				})
				.catch(error => console.warn(error.message));
		},

		AudioPlay(event) {
			let audio = document.querySelectorAll('.call_discuss_rec_play audio');
			for (let i = 0; i < audio.length; i++) {
				if (audio[i] === event.target) continue;
				audio[i].pause();
			}
		},
	},
}