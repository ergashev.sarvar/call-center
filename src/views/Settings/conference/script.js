export default {
    data() {
        return{
            conferencePages: [
                {
                    conference_name: '',
                    run_time: 0
                }
            ],
            members: [],
            selectedConference: '',
            conferenceName: '',
            actionDone: '',
            run_time: 0
        }
    },
    methods: {
        getConferencePages() {
            axios.get('/IvrFs/FsJson?command=%20%20fs_cli%20-x%20%22conference%20json_list%22')
                .then(response => {
                    this.conferencePages = response.data;
                })
        },
        uni1(method, member_id) {
            axios.get(`/IvrFs/RunFsCommand?command=%20%20fs_cli%20-x%20%22conference%20${this.conferenceName}%20${method}%20${member_id}%22`)
                .then(response => {
                    this.getConferencePages();
                })
        },
        uni2(method, inputData, ending){
            axios.get(`/IvrFs/RunFsCommand?command=%20%20fs_cli%20-x%20%22conference%20${this.conferenceName}%20${method}%20${inputData}%20${ending}%22`)
                .then(response => {
                    this.getConferencePages();
                })
        },
        bottomActions(){
            var _action = this.$refs.chooseCmd.value;
            var _input = this.$refs.inputData.value;
            this.uni1(_action, _input.replace(' ', '%20'));
        },
        confAction(method){
            axios.get(`/IvrFs/RunFsCommand?command=%20%20fs_cli%20-x%20%22conference%20${this.conferenceName}%20${method}%22`)
                .then(response => {
                    this.getConferencePages();
                })
        }
    },
    mounted() {
        axios.get('/IvrFs/FsJson?command=%20%20fs_cli%20-x%20%22conference%20json_list%22')
            .then(response => {
                this.conferencePages = response.data;
                this.selectedConference = response.data.length > 0 ? response.data[0].conference_name : '';
                this.run_time = response.data.length > 0 ? response.data[0].run_time : 0;
            });
        setInterval(() => {
            this.getConferencePages();
        }, 3000);
    },
    watch: {
        selectedConference: function (val) {
            if (val != '') this.members = this.conferencePages.find(page => page['conference_name'] === val)['members'];
            this.conferenceName = val;
        },
        conferencePages: function (val) {
            if (val != '') this.members = val.find(page => page['conference_name'] === this.conferenceName)['members'];
        },
        run_time: function(val){
            if (this.conferenceName != '') this.run_time = val;
        }
    }
}
//