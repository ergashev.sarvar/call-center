export default {
	name: "index",
	data() {
		return {
			modelList: [],
			actionType: null,
			id: null,
			request: {
				text: '',
				orgId: null,
			}
		}
	},
	methods: {
		getList() {
			this.$http.get('Playmobile/GetDeals').then(res => {
				this.modelList = res.data.deals;
			})
		},
		readDeal(id) {
			this.id = id;
			this.actionType = 2;
			this.$http.get('Playmobile/GetDeal', {params: {id: id}}).then(res => {
				this.request = res.data.deal
			})
		},
		addDeal() {
			this.request.orgId = parseInt(localStorage.getItem("organization_id"));
			this.$http.post('Playmobile/AddDeal', this.request).then(res => {
				if (res && res.data.success) {
					$('#modalInfo').modal('hide');
					this.getList();
				}
			})
		},
		updateDeal() {
			this.request.id = this.id;
			this.$http.put('Playmobile/UpdateDeal', this.request).then(res => {
				if (res && res.data.success) {
					$('#modalInfo').modal('hide');
					this.getList();
				}
			})
		},
		deleteDeal(id) {
			if (confirm("Do you actually delete this item?")) {
				this.$http.delete('Playmobile/DeleteDeal', {params: {id: id}}).then(res => {
					this.$store.state.notifyDelete();
					this.getList();
				})
			}
		},
		resetForm() {
			this.request.text = '';
			delete this.request.id;
		}
	},
	created() {
		this.getList();
	}
}