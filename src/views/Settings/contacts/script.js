export default {
	component: {},
	data() {
		return {
			request: {
				position: '',
				number: '',
				numberInside: '',
				orgId: null
			},
			modelList: [],
			actionType: null,
			id: null,
		}
	},
	methods: {
		getList() {
			this.$http.get('Playmobile/GetContacts').then(res => {
				this.modelList = res.data.contactInfo;
			})
		},
		addContact() {
			this.request.orgId = parseInt(localStorage.getItem("organization_id"));
			this.$http.post('Playmobile/AddContact', this.request).then(res => {
				if (res && res.data.success) {
					$('#modalInfo').modal('hide');
					this.getList();
				}
			})
		},
		updateContact() {
			this.request.id = this.id;
			this.$http.put('Playmobile/EditContact', this.request).then(res => {
				if (res && res.data.success) {
					$('#modalInfo').modal('hide');
					this.getList();
				}
			})
		},
		readContact(id) {
			this.id = id;
			this.actionType = 2;
			this.$http.get('Playmobile/GetContact/' + id).then(res => {
				this.request = res.data.contactInfo
			})
		},
		deleteContact(id) {
			if (confirm("Do you actually delete this item?")) {
				this.$http.delete('Playmobile/RemoveContact/' + id).then(res => {
					this.$store.state.notifyDelete();
					this.getList();
				})
			}
		},
		resetForm() {
			this.request.position = '';
			this.request.number = '';
			this.request.numberInside = '';
			this.request.orgId = null;
		}
	},
	created() {
		this.getList();
	}
}