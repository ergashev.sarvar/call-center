export default {
	Create(model) {
		return new Promise((result, reject) => {
			axios.post('/Admin/role/module', model)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	Read(id) {
		return new Promise((result, reject) => {
			axios.get('/Admin/role/modules/' + id)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	Delete(id) {
		return new Promise((result, reject) => {
			axios.delete('/Admin/role/module/' + id)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetModules() {
		return new Promise((result, reject) => {
			axios.get('/Admin/modules')
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetActions() {
		return new Promise((result, reject) => {
			axios.get('/Admin/actions')
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},
}