import RoleCRUD from './RoleCRUD'
import RoleUserCRUD from './RoleUserCRUD'
import RoleModuleCRUD from './RoleModuleCRUD'
import Paginate from "vuejs-paginate";

export default {
	data() {
		return {
			roleDetailsId: 0,
			pageCount: 0,
			roleType: 1,
			roleActionType: 0,
			roleList: [],
			roleModel: {},

			roleUserList: [],
			roleModuleList: [],
			roleSelected: false,

			userList: [],
			userSelected: {},
			moduleList: [],
			moduleSelected: {},
			actionList: [],
			actionSelected: {},

			errorMessage: '',
		}
	},
	components: {
		Paginate,
	},

	methods: {
		onSubmit(event) {
			event.preventDefault()
		},
		RoleCreate() {
			if (!this.roleModel.name || this.roleModel.name.trim() === '') {
				return;
			}
			RoleCRUD.Create(this.roleModel)
				.then(result => {
					console.log(result);

					if (result.success) {
						$('#roleModal').modal('hide');
						this.RoleGetAll();
					}
					else {
						console.log(result.message);
					}
				});
		},
		RoleRead(id) {
			RoleCRUD.Read(id)
				.then(result => {
					// console.log(result);

					if (result.success) {
						this.roleModel = result.data;
						$('#roleModal').modal('show');
					}
					else {
						console.log(result.message);
					}
				});
		},
		RoleUpdate() {
			if (!this.roleModel.name || this.roleModel.name.trim() === '') {
				return;
			}
			RoleCRUD.Update(this.roleModel)
				.then(result => {
					// console.log(result);

					if (result.success) {
						$('#roleModal').modal('hide');
						this.RoleGetAll();
					}
					else {
						console.log(result.message);
					}
				});
		},
		RoleDelete() {
			RoleCRUD.Delete(this.roleModel.id)
				.then(result => {
					// console.log(result);

					if (result.success) {
						$('#roleModal').modal('hide');
						this.RoleGetAll();
					}
					else {
						console.log(result.message);
					}
				});
		},
		RoleGetAll() {
			RoleCRUD.GetAll()
				.then(result => {
					// console.log(result);

					if (result.success) {
						this.roleList = result.data;
					}
					else {
						console.log(result.message);
					}
				});
		},


		RoleDetails(page = 0) {
			let id = this.roleDetailsId
			if (page > 0) {
				page = page - 1;
			}
			RoleUserCRUD.Read(page, id)
				.then(result => {
					console.log(result);
					if (result.success) {
						this.roleUserList = result.data.users;
						this.pageCount = result.data.pages;
					}
					else {
						console.log(result.message);
					}
				});

			RoleModuleCRUD.Read(id)
				.then(result => {
					// console.log(result);

					if (result.success) {
						this.roleModuleList = result.data;
					}
					else {
						console.log(result.message);
					}
				});

			RoleCRUD.Read(id)
				.then(result => {
					// console.log(result);

					if (result.success) {
						this.roleSelected = result.data;
					}
					else {
						console.log(result.message);
					}
				});
		},


		ListUsers() {
			RoleUserCRUD.GetAllUsers()
				.then(result => {
					if (result.success) {
						this.userList = result.data;
					}
					else {
						console.log(result.message);
					}
				});
		},
		RoleUserCreate() {
			RoleUserCRUD.Create({
				"roleId": this.roleSelected.id,
				"userId": parseInt(this.userSelected),
				})
				.then(result => {
					// console.log(result);

					if (result.success) {
						$('#userModuleModal').modal('hide');
						// this.RoleDetails(this.roleSelected.id);
						this.RoleDetails();
						this.RoleGetAll();
					}
					else {
						// console.log(result.message);
						this.errorMessage = result.message;
					}
				});
		},
		RoleUserDelete(id) {
			if (confirm('You are about to delete the record! \nAre you sure?')) {
				RoleUserCRUD.Delete(id)
					.then(result => {
						// console.log(result);

						if (result.success) {
							// this.RoleDetails(this.roleSelected.id);
							this.RoleDetails();
							this.RoleGetAll();
						}
						else {
							console.log(result.message);
						}
					});
			}
		},


		ListModuleAction() {
			RoleModuleCRUD.GetModules()
				.then(result => {
					// console.log(result);

					if (result.success) {
						this.moduleList = result.data;
					}
					else {
						console.log(result.message);
					}
				});

			RoleModuleCRUD.GetActions()
				.then(result => {
					// console.log(result);

					if (result.success) {
						this.actionList = result.data;
					}
					else {
						console.log(result.success);
					}
				});
		},
		RoleModuleCreate() {
			RoleModuleCRUD.Create({
				"roleId": this.roleSelected.id,
				"moduleId": parseInt(this.moduleSelected),
				"actionId": parseInt(this.actionSelected),
			})
				.then(result => {
					// console.log(result);

					if (result.success) {
						$('#userModuleModal').modal('hide');
						this.RoleDetails(this.roleSelected.id);
						this.RoleGetAll();
					}
					else {
						this.errorMessage = result.message;
					}
				});
		},
		RoleModuleDelete(id) {
			if (confirm('You are about to delete the record! \nAre you sure?')) {
				RoleModuleCRUD.Delete(id)
					.then(result => {
						// console.log(result);

						if (result.success) {
							this.RoleDetails(this.roleSelected.id);
							this.RoleGetAll();
						}
						else {
							console.log(result.message);
						}
					});
			}
		},
	},


	mounted() {

		this.RoleGetAll();


		$('#roleModal').on('hidden.bs.modal', (e) => {
			this.roleModel = {};
			this.errorMessage = '';
		});

		$('#userModuleModal').on('hidden.bs.modal', (e) => {
			this.userSelected = '';
			this.moduleSelected = '';
			this.actionSelected = '';
			this.errorMessage = '';
		});

	}
}