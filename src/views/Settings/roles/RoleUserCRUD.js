export default {
	Create(model) {
		return new Promise((result, reject) => {
			axios.post('/Admin/role/user', model)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	Read(page, id) {
		return new Promise((result, reject) => {
			axios.get('/Admin/role/users/' + id + '?page=' + page)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	Delete(id) {
		return new Promise((result, reject) => {
			axios.delete('/Admin/role/user/' + id)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetAllUsers() {
		return new Promise((result, reject) => {
			axios.get('/Admin/users')
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},
}