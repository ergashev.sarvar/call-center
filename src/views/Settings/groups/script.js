import Form from '../../../models/Form';

export default {
	data() {
		return {
			groups: [],
			users: [],
			filteredUsers: [],
			id: null,
			group: {},
			groupForm: new Form({
				name: '',
				id: null
			}),
			userForm: new Form({
				users: [],
				groupId: null,
				id: null,
			}),
			usersSearch: '',
			allUsers: [],
			deleteGroupState: null,
			deleteUserState: null,
			selectedUsersGroup: null
		}
	},
	methods: {
		getGroups() {
			this.groupForm.submit('get', '/timesheetadmin/group')
			  .then(response => {
				  if (response.success === true) {
					  this.groups = response.groups;
				  }
			  })
		},
		getUsers(group) {
			this.selectedUsersGroup = {...group}
			this.groupForm.submit('get', '/timesheetadmin/users/' + group.id)
			  .then(res => {
				  if (res.success === true) {
					  this.group = group;
					  this.id = group.id;
					  this.users = res.usersInfos;
					  this.filteredUsers = this.allUsers.filter(item => {
						  let it = this.users.find(second => second.userId === item.id);
						  if (!it) {
						  	return item;
						  }
					  });
				  }
			  })
		},
		saveGroup() {
			if (this.groupForm.id > 0) {
				this.updateGroup();
			} else {
				this.addGroup();
			}
		},
		addGroup() {
			delete this.groupForm.id;
			this.groupForm.submit('post', '/timesheetadmin/group')
			  .then(response => {
				  this.getGroups()
				  this.$store.state.notifyAdd();
				  $('#groupModal').modal('hide');
			  })
		},
		updateGroup() {
			this.groupForm.submit('patch', '/timesheetadmin/group')
			  .then(response => {
				  this.$store.state.notifyEdit();
				  this.groups.find(item => item.id === this.groupForm.id).name = this.groupForm.name;
				  $('#groupModal').modal('hide');
			  })
		},
		appendToForm(user) {
			this.userForm.users.push({
				userId: user.id,
				groupId: this.group.id
			});
		},
		saveUsers() {
			axios.post('/timesheetadmin/usersinfos', this.userForm.users)
			  .then(response => {
				  this.filteredUsers = [];
				  this.getGroups();
				  this.getAllUsers()
				  this.getUsers( this.selectedUsersGroup )
				  this.$store.state.notifyAdd();
			  })
		},
		deleteGroup() {
			this.groupForm.submit('delete', '/timesheetadmin/group/' + this.deleteGroupState.id)
			  .then(res => {
				  if (res.success) {
					  let indx = this.groups.indexOf(this.deleteGroupState);
					  this.groups.splice(indx, 1);
					  this.deleteGroupState = null
					  this.getGroups();
					  $('#groupDeleteModal').modal('hide');
					  this.$store.state.notifyDelete();
				  }
			  })
		},
		deleteUser() {
			this.userForm.submit('delete', '/timesheetadmin/usersinfo/' + this.deleteUserState.id)
			  .then(res => {
				  this.users = this.users.filter(obj => obj.id !== this.deleteUserState.id);
				  this.deleteUserState = null
				  this.getGroups();
				  $('#userDeleteModal').modal('hide');
				  this.$store.state.notifyDelete();
			  })
		},
		filterUsers() {
			this.filteredUsers = this.allUsers.filter(item => item.fio.indexOf(this.usersSearch) >= 0);
		},
		setGroupData(group) {
			this.groupForm.setData(group);
			this.userForm.groupId = group.id;
		},
		getAllUsers() {
			axios.get('/admin/users')
			  .then(response => {
				  this.allUsers = response.data.data;
				  this.filteredUsers = this.allUsers;
			  })
		}
	},
	created() {
		this.getGroups();
		this.getAllUsers()
	}
}