import CKEditor from '@ckeditor/ckeditor5-vue';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Datepicker from "vue-bootstrap-datetimepicker";
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';
import VueTags from "vue-tags";
import Phone from '@/components/Phone/Phone.vue';
import {mapState} from 'vuex';

export default {
	name: "crm2",
	components: {
		ckeditor: CKEditor.component,
		Datepicker: Datepicker,
		VueTags: VueTags,
		Phone
	},
	data() {
		return {
			nameAll: '',
			editor: ClassicEditor,
			editorConfig: {},
			introList: [],
			contactsList: [],
			opersList: [],
			membersList: [],
			faqCategory: [],
			dealList: [],
			faq: [],
			faqShown: false,
			search: '',
			action: null,
			userPhone: null,
			userEmail: '',
			isInput: null,
			historyList: [],
			categoryList: [],
			tags: [],
			statusList: [],
			departmentList: [],
			request: {
				categoryIds: [],
				deadline: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
				departmentIds: [],
				tags: [],
				status: null,
				notes: '',
				orgId: null,
                phoneNumber: null

			},
			options: {
				format: 'YYYY-MM-DD HH:mm:ss',
				useCurrent: false,
				showClear: true,
				showClose: true,
				locale: 'ru',
				icons: {
					time: 'mdi mdi-clock-outline',
					date: 'mdi mdi-calendar',
					up: 'mdi mdi-arrow-up',
					down: 'mdi mdi-arrow-down',
					previous: 'mdi mdi-arrow-left',
					next: 'mdi mdi-arrow-right',
					today: 'mdi mdi-calendar-today',
					clear: 'mdi mdi-delete-outline',
					close: 'mdi mdi-alpha-x-circle-outline'
				}
			},
			org: []


		}
	},
	methods: {
		getGreeting() {
			this.$http.get('Playmobile/Get').then(res => {
				this.introList = res.data.introductions;
			})
		},
		getContacts() {
			this.$http.get('Playmobile/GetContacts').then(res => {
				this.contactsList = res.data.contactInfo
			})
		},
		getOpers() {
			this.$http.get('Dashboard/GetOpersByStatus').then(res => {
				this.opersList = res.data;
			})
		},
		getMembers() {
			this.$http.get('Dashboard/CallCenterMembers').then(res => {
				this.membersList = res.data.members;
			})
		},
		getFaqCategory() {
			this.$http.get('Playmobile/GetFaqCategories').then(res => {
				this.faqCategory = res.data.categories;
			})
		},
		getFaq(id) {
			this.faqShown = true;
			this.$http.get('Playmobile/GetFaqs/categoryId', {params: {categoryId: id}}).then(res => {
				this.faq = res.data.faqs;
			})
		},
		closeBar() {
			this.faqShown = false;
		},
		getDeal() {
			this.$http.get('Playmobile/GetDeals').then(res => {
				this.dealList = res.data.deals;
			})
		},
		send(text, action) {
			if (action === 1) {
				this.$http.get('Playmobile/SendTelegram',
					{
						params: {
							firstName: "Test",
							lastName: "Test",
							phone: this.userPhone,
							message: text
						}
					}).then(res => {
					this.$store.state.notifySend();
				})
			} else if (action === 2) {
				this.$http.get('Playmobile/SendEmail',
					{
						params:
							{
								text: text,
								email: this.userEmail
							}
					}).then(res => {
					this.$store.state.notifySend();
				})
			} else {
				this.$http.post('ClientCard/smsSend', {},
					{
						params: {
							phone: this.userPhone,
							text: text
						}
					}).then(res => {
					this.$store.state.notifySend();
					console.log(this.userPhone);
				})
			}
		},
		getHistory() {
			let orgId = parseInt(localStorage.getItem("organization_id"));
			this.$http.get('ClientCard/clientCardPlayMobile/' + orgId, {params: {page: 0}}).then(res => {
				this.historyList = res.data.clientCards;
				this.statusList = res.data.statuses;
				this.categoryList = res.data.categories
			})
		},
		statusGet(status) {
			let stat = this.statusList.find(res => res.id == status);
			return stat[`status${this.lang}`] ? stat[`status${this.lang}`] : '';
		},
		getCat(cat) {
			let result = [];
			cat.forEach((id, index) => {
				result[index] = ((this.categoryList.find(item => item.id === parseInt(id)) || {name: ''}).name);
			});
			return result.join(', ');
		},
		getDepartment() {
			this.$http.get('Admin/department').then(res => {
				this.departmentList = res.data.data;
			})
		},
		saveTicket() {
			this.request.categoryIds = this.request.categoryIds.length > 0 ? this.request.categoryIds.map(x => x.id.toString()) : [];
			this.request.departmentIds = this.request.departmentIds.length > 0 ? this.request.departmentIds.map(x => x.id.toString()) : [];
			this.request.orgId = this.request.orgId = parseInt(localStorage.getItem("organization_id"));
			this.request.phoneNumber = localStorage.getItem("caller_id");
			this.$http.post('ClientCard/clientCardPlayMobile', this.request).then(res => {
				if (res && res.data.success) {
					this.resetForm();
					this.$store.state.notifySave();
					this.getHistory();

				}
			})
		},
		saveTags() {
			let res = {
				notes: this.request.notes,
				tags: this.request.tags
			};
			this.$http.post('ClientCard/clientCardPlayMobile', res).then(res => {
				if (res && res.data.success) {
					this.resetForm();
					this.$store.state.notifySave();
					this.getHistory();

				}
			})
		},
		resetForm() {
			this.request.categoryIds = [];
			this.request.deadline = null;
			this.request.departmentIds = [];
			this.request.tags = [];
			this.request.status = null;
			this.request.notes = '';
		},
		getOrg() {
			let orgId = parseInt(localStorage.getItem("organization_id"));
			this.$http.get('Admin/organization/' + orgId).then(res => {
				this.org = res.data.data
			})
		},
		call(number) {
			this.$store.commit("changeCrmPhone", number);
		},
		getInfo(){
			this.$http.get('Dashboard/OrganizationByNumber', {params:{phone: localStorage.getItem('caller_id')}}).then(res => {
				this.org = res.data.organization;
                this.historyList = res.data.clientCards.clientCards;
			})
		}
	},
	created() {
		this.getGreeting();
		this.getContacts();
		this.getOpers();
		this.getMembers();
		this.getFaqCategory();
		this.getDeal();
		this.getHistory();
		this.getDepartment();
		this.getOrg();
		setInterval(() => {
			this.getMembers();
			this.getOpers()
		}, 5000);
	},
	computed: {
		lang() {
			return this.$store.state.lang.replace(/\b\w/g, l => l.toUpperCase())
		},
		filteredClients() {
			if (this.search) {
				return this.faq.filter((item) => {
					return item.question.toLowerCase().indexOf(this.search.toLowerCase()) > -1
				})
			} else {
				return this.faq;
			}
		},
	},
	mounted() {
		this.userPhone = ((this.$store.state.caller_id !== null) && (this.$store.state.caller_id.length > 9)) ? this.$store.state.caller_id.slice(4) : this.$store.state.caller_id;
		setTimeout(() => {
			this.getMembers();
			this.getOpers()
		}, 5000);
	},
	watch: {
		// '$store.state.crmPhone'(val)
        '$store.state.isCalling'(val) {
            // this.$store.state.isCalling = false;
            this.getInfo();
        }

    }
}