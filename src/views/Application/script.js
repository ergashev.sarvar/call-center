import Datepicker from "vue-bootstrap-datetimepicker";
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';
import Paginate from 'vuejs-paginate';

export default {
	name: "index",
	components: {Datepicker, Paginate},
	data() {
		return {
			options: {
				format: 'YYYY-MM-DD HH:m:s',
				useCurrent: false,
				showClear: true,
				showClose: true,
				locale: 'ru',
				icons: {
					time: 'mdi mdi-clock-outline',
					date: 'mdi mdi-calendar',
					up: 'mdi mdi-arrow-up',
					down: 'mdi mdi-arrow-down',
					previous: 'mdi mdi-arrow-left',
					next: 'mdi mdi-arrow-right',
					today: 'mdi mdi-calendar-today',
					clear: 'mdi mdi-delete-outline',
					close: 'mdi mdi-alpha-x-circle-outline'
				}
			},
			fromDate: '',
			toDate: '',
			status: null,
			caller: null,
			content: null,
			operator: null,
			page: 1,
			allCount: null,
			cards: []
		}
	},
	methods: {
		getAllCards() {
			this.$http.get('ClientCard/filterCards',
				{
					params: {
						from: this.fromDate,
						to: this.toDate,
						caller: this.caller,
						oper: this.operator,
						status: this.status,
						content: this.content,
						page: this.page
					}
				}).then(res => {
				this.allCount = res.data.count;
				this.cards = res.data.clientCards;
			})
		},
		filer() {
			this.page = 1;
			this.getAllCards();
		},
		paginateData(page) {
			this.page = page;
			this.getAllCards();
			this.animateScrollToTop();
		},
		animateScrollToTop() {
			window.scroll({
				top: 0,
				left: 0,
				behavior: 'smooth'
			});
		},
	},
	created() {
		this.getAllCards();
	},
	computed: {
		pagesCount: function () {
			return Math.ceil(this.allCount / 15);
		},
	}
}