export default {
	data() {
		return {
			serverId: '',
			activationKey: '',

			status: 2,
		}
	},
	methods: {
		Activate() {
			axios.post('/Login/activate', {
					"key": this.activationKey
				})
				.then(response => {
					if (response.data.success) {
						localStorage.activated = 1;
						this.status = 1;
					}
					else {
						this.status = 0;
					}
				})
				.catch(error => {
					console.log(error.message);
				});
		}
	},
	mounted() {

		if (localStorage.getItem('activated') === null) {
			axios.get('/Login/servercode')
				.then(response => {
					this.serverId = response.data;
				})
				.catch(error => {
					console.log(error.message);
				});
		}
		else {
			axios.get('/Login/ActivationInfo')
				.then(response => {
					this.serverId = response.data.data.serverCode;
					this.activationKey = response.data.data.activationKey;
					this.status = 1;
				})
				.catch(error => {
					console.log(error.message);
				});
		}
	}
}