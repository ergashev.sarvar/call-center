import VueApexCharts from 'vue-apexcharts'
import chartSettings from './chartSettings'
import RoleCRUD from "../Settings/modes/ModeCRUD";


export default {
	components: {
		apexchart: VueApexCharts,
	},
	data() {
		return {
			chart: chartSettings,
			activities: null,
			reviews: null,
			selected1: 1,
			selected2: 2,
			selectedStatus: 'Выберите значение',
			agentsStatuses: ['Logged Out', 'Available', 'Available (On Demand)', 'On Break', 'All'],
			agentsActivitiesStatic: null,
			agentsActivities: null,
			agentsList: [],
			selectedAgentNameId: 'all',
			date: ( new Date() ).getDate(),
			users: [],


			queues: [],
			lastHandledCalls: [],
			lastRejectedCalls: [],
			topOperators: [],

			updateInterval: 0,
		}
	},
	created() {
		this.getReport();
		this.getUsers();

		this.GetLastHandledCalls();
		this.GetLastRejectedCalls();
		this.GetTopOperators();


		this.getMetric();
		this.GetQueue();
		this.GetActivityOfOperator();

		this.updateInterval = setInterval(() => {
			this.getMetric();
			this.GetQueue();
			this.GetActivityOfOperator();
		}, 3000);
	},
	beforeMount() {
		this.reviews = this.reviews ? this.reviews.revert() : this.reviews;
	},
	beforeDestroy() {
		clearInterval(this.updateInterval);
	},
	mounted() {
		$('[data-toggle="tooltip"]').tooltip();
		$('.selectpicker').selectpicker();
	},
	filters: {
		countSeries: function (arr) {
			let result = 0;
			arr.forEach(item => {
				if (!isNaN(item.y)) {
					result = item.y + result
				} else {
					let timeArr = item.y.split(':');
					let timeSeconds = timeArr[0] * 3600 + timeArr[1] * 60 + +timeArr[2];
					if (result !== 0) {
						let resultArr = result.split(':')
						let resultSeconds = resultArr[0] * 3600 + resultArr[1] * 60 + +resultArr[2];
						timeSeconds = timeSeconds + resultSeconds
					}
					result = `${Math.floor(timeSeconds/3600) > 10 ? Math.floor(timeSeconds/3600): '0' + Math.floor(timeSeconds/3600)}:${Math.floor(timeSeconds%3600/60) > 10 ? Math.floor(timeSeconds%3600/60) : '0' + Math.floor(timeSeconds%3600/60)}:${timeSeconds%60 > 10 ? timeSeconds%60 : '0' + timeSeconds%60}`;
				}
			});
			return result;
		},
		readyTime: function (val) {
			let a = new Date(val * 1000);
			let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
			let year = a.getFullYear();
			let month = months[a.getMonth()];
			let date = a.getDate();
			let hour = a.getHours();
			let min = a.getMinutes();
			let sec = a.getSeconds();
			let time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
			return time;
		},
	},
	methods: {
		prevDef: function (event) {
			event.preventDefault()
		},
		drawChart() {
			let item1 = this.reviews.find(item => item.selectedValue === this.selected1);
			let item2 = this.reviews.find(item => item.selectedValue === this.selected2);

			this.chart.series[0].name = item1.name;
			this.chart.series[0].data = item1.series;
			this.chart.series[1].name = item2.name;
			this.chart.series[1].data = item2.series;

			this.$refs.chart.refresh();
		},
		changeStatus(val){
			let newAgentsActivities = [...this.agentsActivitiesStatic];
			if (val === 'All') {
				this.agentsActivities = newAgentsActivities;
				return
			}
			this.agentsActivities = newAgentsActivities.filter(obj => {
				return (
					obj.element.find(item => {
						return item.key === 'status'
					}).value === val
				)
			})
		},
		getMetric() {
			axios
				.get('/dashboard/metric')
				.then(response => {
					this.activities = response.data;
				})
				.catch(error => {
					console.log(error);
				})
				.finally(() => (this.loading = false));
		},
		getReport() {
			axios
				.get('/dashboard/report?dayCount=' + this.date)
				.then(response => {
					let newReviews = response.data;
					newReviews.forEach((value, index, arr) => {
						arr[index]['selectedValue'] = index + 1
					});
					this.reviews = newReviews;

					this.drawChart();
					$('.selectpicker').selectpicker();
				})
				.catch(error => {
					console.log(error);
				})
				.finally(() => (this.loading = false));
		},
		getReportByAgent() {
			if (!this.selectedAgentNameId) {
				return
			}

			if (this.selectedAgentNameId === 'all') {
				this.getReport();
				return
			}
			axios
				.get(`/Dashboard/ReportByAgent?agentId=${this.selectedAgentNameId}&dayCount=${this.date}`)
				.then(response => {

					let newReviews = response.data;
					newReviews.forEach((value, index, arr) => {
						arr[index]['selectedValue'] = index + 1
					});

					this.reviews = newReviews;
					this.drawChart();
					$('.selectpicker').selectpicker();
				})
				.catch(error => {
					console.log(error);
				})
		},
		getUsers() {
			axios.get('/admin/users')
				.then(response => {
					this.users = response.data.data;
				})
		},



		GetActivityOfOperator() {
			axios
				.get('/IvrFs/RunFsCommand?command=fs_cli%20-x%20%22callcenter_config%20agent%20list%22')
				.then(response => {
					// console.log(response.data);

					this.agentsActivities = [...response.data.result];
					this.agentsActivitiesStatic = [...response.data.result];
				})
				.catch(error => {
					console.log(error.message);
				});
		},

		GetQueue() {
			axios.get('IvrFs/RunFsCommand?command=fs_cli%20-x%20%22callcenter_config%20queue%20list%20members%22')
				.then(response => {
					// console.log(response.data.result);

					this.queues = response.data.result;
				})
				.catch(error => {
					console.log(error);
				});
		},
		GetLastHandledCalls() {
			axios.get('Dashboard/GetLastHandledCalls/' + 10)
				.then(response => {
					this.lastHandledCalls = response.data;
				})
				.catch(error => {
					console.log(error);
				});
		},
		GetLastRejectedCalls() {
			axios.get('Dashboard/GetLastRejectedCalls/' + 10)
				.then(response => {
					this.lastRejectedCalls = response.data;
				})
				.catch(error => {
					console.log(error);
				});
		},
		GetTopOperators() {
			axios.get('Dashboard/GetTopOperators/' + 10)
				.then(response => {
					this.topOperators = response.data;
				})
				.catch(error => {
					console.log(error);
				});
		},

		CallSpy(callUuid) {
			axios
				.get('Spy/Park')
				.then(response => {
					// console.log(response.data);

					if (response.data.success) {

						axios
							.get('Spy/Eavesdrop/' + response.data.result.uuid + '/' + callUuid)
							.then(response2 => {
								console.log(response2.data);
							})
							.catch(error2 => {
								console.log(error2.message);
							});
					}
					else {
						console.log(response.data.success);
					}
				})
				.catch(error => {
					console.log(error.message);
				});
		},
	},
	watch: {
		selected1: function (val) {
			this.drawChart();
		},
		selected2: function (val) {
			this.drawChart();
		},
		selectedStatus: function (val) {
			this.changeStatus(val)
		}
	}
}