export default {
	GetTicketsNew(model) {
		return new Promise((result, reject) => {
			axios
				.get('ClientCard/clientCardsIncomeFiltered', {
					params: model
				})
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},

	GetTicketsOld(model) {
		return new Promise((result, reject) => {
			axios
				.get('ClientCard/clientCardsOutFiltered', {
					params: model
				})
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},



	GetDistrict(cityId) {
		return new Promise((result, reject) => {
			axios.get('Hokimiyat/District/' + cityId)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetStreet(districtId) {
		return new Promise((result, reject) => {
			axios.get('Hokimiyat/Street/' + districtId)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetHouse(streetId) {
		return new Promise((result, reject) => {
			axios.get('Hokimiyat/House/' + streetId)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},
}