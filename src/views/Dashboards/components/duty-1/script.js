import Paginate from 'vuejs-paginate';
import Form from '@/models/Form.js';
import CRUD from './CRUD';



export default {
	components: {
		Paginate,
	},
	data() {
		return {
			ticketNewList: [],
			ticketNewListPageCount: 0,

			ticketOldList: [],
			ticketOldListPageCount: 0,


			userList: [],
			categoryList: [],
			priorityList: [],
			statusList: [],


			cityList: [
				{
					id: 1,
					name: 'Tashkent'
				},
				{
					id: 2,
					name: 'Termez'
				},
				{
					id: 3,
					name: 'Bukhara'
				},
				{
					id: 4,
					name: 'Samarkand'
				},
			],
			districtList: [],
			streetList: [],
			houseList: [],


			model: new Form({
				cityId: 0,
				regionId: 0,
				streetId: 0,
				houseId: 0,
			}),

			intervalID: 0,
		}
	},
	methods: {
		GetTicketsNew() {
			// console.log(this.model.data());

			CRUD.GetTicketsNew(this.model.data()).then(result => {
				// console.log(result);

				if (result.success) {
					this.userList = result.users;
					this.categoryList = result.categories;
					this.priorityList = result.priorities;
					this.statusList = result.statuses;

					this.ticketNewList = result.clientCards;
					this.ticketNewListPageCount = Math.ceil(result.count / 10);
				}
				else {
					console.log(result.success);
				}
			});
		},

		GetTicketsOld() {
			// console.log(this.model.data());

			CRUD.GetTicketsOld(this.model.data()).then(result => {
				// console.log(result);

				if (result.success) {
					this.userList = result.users;
					this.categoryList = result.categories;
					this.priorityList = result.priorities;
					this.statusList = result.statuses;

					this.ticketOldList = result.clientCards;
					this.ticketOldListPageCount = Math.ceil(result.count / 10);
				}
				else {
					console.log(result.success);
				}
			});
		},




		SelectTree(event) {
			let index_current = [...document.querySelectorAll('.select_tree')].indexOf(event.target);

			switch(index_current) {
				case 0:
					// console.log('City is selected');
					// console.log(event.target.value);

					this.model.regionId = 0;
					this.model.streetId = 0;
					this.model.houseId = 0;
					CRUD.GetDistrict(event.target.value).then(result => {
						// console.log(result);

						if (result.success) {
							this.districtList = result.districts;
						}
						else {
							console.log(result.message);
						}
					});
					break;

				case 1:
					// console.log('District is selected');
					// console.log(event.target.value);

					this.model.streetId = 0;
					this.model.houseId = 0;
					CRUD.GetStreet(event.target.value).then(result => {
						// console.log(result);

						if (result.success) {
							this.streetList = result.streets;
						}
						else {
							console.log(result.message);
						}
					});
					break;

				case 2:
					// console.log('Street is selected');
					// console.log(event.target.value);

					this.model.houseId = 0;
					CRUD.GetHouse(event.target.value).then(result => {
						// console.log(result);

						if (result.success) {
							this.houseList = result.houses;
						}
						else {
							console.log(result.message);
						}
					});
					break;

				case 3:
					// console.log('House is selected');
					// console.log(event.target.value);

					break;
			}


			[...document.querySelectorAll('.select_tree')].forEach((item, index, arr) => {
				if (event.target.value === '0') {
					item.disabled = index > index_current;
				}
				else {
					item.disabled = index > index_current + 1;
				}
			});
		},

		Filter() {
			this.GetTicketsNew();
			this.GetTicketsOld();

			clearInterval(this.intervalID);
			this.intervalID = setInterval(() => {
				this.GetTicketsNew();
				this.GetTicketsOld();
			}, 5000);
		}
	},
	mounted() {
		this.Filter();
	},
}