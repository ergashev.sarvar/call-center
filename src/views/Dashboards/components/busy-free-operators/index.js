import VueApexCharts from 'vue-apexcharts';

export default {
	components: {
		apexchart: VueApexCharts,
	},
	data: () => ({
		options: {
			chart: {
				type: 'radialBar'
			},
			plotOptions: {
				radialBar: {
					startAngle: -90,
					endAngle: 90,
					track: {
						background: '#E4ECF2'
					},
					dataLabels: {
						name: {
							show: false
						},
						value: {
							offsetY: 0,
							fontSize: '22px'
						}
					}
				},
			},
			fill: {
				type: 'gradient',
				colors: ['#42CCEB'],
			},
			labels: [],
		},
		members: [],
		waitingOptions: {},
		receiveOptions: {},
		bronOptions: {},
		waitingSeries: [0],
		receiveSeries: [0],
		bronSeries: [0],
		operatorRatings: [],
		lastReceivedCalls: [],
		lastRejectedCalls: [],
		avialableAgents: [],
		busyAgents: [],
		deleteInterval: 0
	}),
	created() {
		this.makeWaitingOptions();
		this.makeReceiveOptions();
		this.makeBronOptions();
		this.init();
	},
	mounted() {
		this.deleteInterval = setInterval(this.init, 10000);
	},
	beforeDestroy(){
		clearInterval(this.deleteInterval);
	},
	methods: {
		makeWaitingOptions() {
			this.waitingOptions = JSON.parse(JSON.stringify(this.options));
		},
		makeReceiveOptions() {
			this.receiveOptions = JSON.parse(JSON.stringify(this.options));
			this.receiveOptions.fill.colors = ['#4286EB'];
			this.receiveOptions.plotOptions.radialBar.track.background = '#F25983';
		},
		makeBronOptions() {
			this.bronOptions = Object.assign({}, this.options);
			this.bronOptions.fill.colors = ['#F25983'];
			this.bronOptions.plotOptions.radialBar.track.background = '#62D166';
		},
		init() {
			this.$http.get('Dashboard/Agents')
				.then(res => {
					if (res.data) {
						this.avialableAgents = res.data.avialableAgents
						this.busyAgents = res.data.busyAgents
						if (!(this.avialableAgents.length === 0 && this.busyAgents.length === 0)) {
							this.bronSeries[0] = Math.floor(this.busyAgents.length / (this.avialableAgents.length + this.busyAgents.length) * 100);
							this.$refs.chart.refresh();
						}
					}
				});
		}
	},
	filters: {
		filterDuration: function (val) {
			let data = moment.duration(val, 'seconds')._data;

			return (data.minutes < 10 ? '0' + data.minutes : data.minutes) +
				':' + (data.seconds < 10 ? '0' + data.seconds : data.seconds);
		},
		filterDate: function (val) {
			let newVal1, newVal2;
			newVal1 = Math.floor(val / 60) < 10 ? ('0' +  Math.floor(val / 60)) : Math.floor(val / 60);
			newVal2 = (val % 60) > 10 ? (val % 60) : ('0' + (val % 60));
			return newVal1 + ' : ' + newVal2;
		}
	}
}