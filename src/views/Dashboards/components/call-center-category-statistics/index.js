import VueApexCharts from 'vue-apexcharts';

Vue.component('apexchart', VueApexCharts);

export default {
    data(){
        return{
            options: {
                chart: {
                    height: 350,
                    type: 'bar',
                },
                colors: ['#F25983', '#42CCEB', '#62D166'],
                plotOptions: {
                    bar: {
                        dataLabels: {
                            position: 'top', // top, center, bottom
                        },
                    }
                },
                dataLabels: {
                    enabled: true,
                    // formatter: function (val) {
                    //     return val + "%";
                    // },
                    offsetY: -20,
                    style: {
                        fontSize: '16px',
                        colors: ['#F25983', '#42CCEB', '#62D166']
                    }
                },
                // markers: {
                //     colors: ['#F25983', '#42CCEB', '#62D166']
                // },
                series: [
                    {
                        name: this.$store.state.locale.accept,
                        data: [3, 4, 4, 10, 4, 3, 3, 2, 1, 5, 2, 8],
                    },
                    {
                        name: this.$store.state.locale.inProcess,
                        data: [1, 6, 8, 3, 9, 8, 6, 7, 2, 2, 4, 5]
                    },
                    {
                        name: this.$store.state.locale.done,
                        data: [2, 5, 6, 7, 1, 5, 8, 4, 6, 8, 6, 2]
                    }
                ],
                xaxis: {
                    categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                    position: 'bottom',
                    labels: {
                        // offsetY: -18,
                    },
                    axisBorder: {
                        show: false
                    },
                    axisTicks: {
                        show: false
                    },
                    crosshairs: {
                        fill: {
                            type: 'gradient',
                            gradient: {
                                colorFrom: '#D8E3F0',
                                colorTo: '#BED1E6',
                                stops: [0, 100],
                                opacityFrom: 0.4,
                                opacityTo: 0.5,
                            }
                        }
                    },
                    tooltip: {
                        enabled: true,
                        offsetY: 0,

                    }
                },
                // fill: {
                //     gradient: {
                //         shade: 'light',
                //         type: "horizontal",
                //         shadeIntensity: 0.25,
                //         gradientToColors: undefined,
                //         inverseColors: true,
                //         opacityFrom: 1,
                //         opacityTo: 1,
                //         stops: [50, 0, 100, 100]
                //     },
                // },
                fill: {
                    colors: ['#F25983', '#42CCEB', '#62D166']
                },
                yaxis: {
                    axisBorder: {
                        show: false
                    },
                    axisTicks: {
                        show: false,
                    },
                    labels: {
                        show: false,
                        // formatter: function (val) {
                        //     return val + "%";
                        // }
                    }

                },
                title: {
                    // text: 'Monthly Inflation in Argentina, 2002',
                    floating: true,
                    offsetY: 320,
                    align: 'center',
                    style: {
                        color: '#444'
                    }
                },
            },
            ticketsReports: [],
			dateList: [],
            districtList: [],
            streetList: [],
            houseList: [],
            flatList: [],
			districtId: null,
			streetId: null,
			houseId: null,
			flatId: null,
			date: '',
			endTime: '',
			interval: 0
        }
    },
    created() {
        this.getDistricts();
        this.refreshData();
        this.interval = setInterval(
			this.refreshData, 10000
		)
    },
    mounted() {
    },
	beforeDestroy(){
		clearInterval(this.interval);
	},
    methods: {
		getDistricts() {
			this.$http.get('Hokimiyat/District/100000')
				.then(res => {
					if (res.data) {
						this.districtList = res.data.districts
					}
				});
		},
		getStreets() {
			this.$http.get('Hokimiyat/Street/' + this.districtId)
				.then(res => {
					if (res.data) {
						this.streetList = res.data.streets
					}
				});
		},
		getHouses() {
			this.$http.get('Hokimiyat/House/' + this.streetId)
				.then(res => {
					if (res.data) {
						this.houseList = res.data.houses
					}
				});
		},
		getFlats() {
			this.$http.get('Hokimiyat/Flat/' + this.houseId)
				.then(res => {
					if (res.data) {
						this.flatList = res.data.flats
					}
				});
		},
		refreshData() {
			let date = this.date ? this.date : moment(new Date()).format('YYYY-MM-DD');
			let endTime = this.endTime ? this.endTime : moment(new Date()).format('YYYY-MM-DD');
			this.$http.get('Dashboard/TicketsReport?startTime=' + date + '&endTime=' + endTime + '&District=' + this.districtId + '&Street=' + this.streetId + '&House=' + this.houseId + '&Flat=' + this.flatId)
				.then(res => {
					if (res.data) {
						this.ticketsReports = res.data.categories
					}
				});
		}
    },
    filters: {
        filterDuration: function (val) {
            let data = moment.duration(val, 'seconds')._data;

            return (data.minutes < 10 ? '0' + data.minutes : data.minutes) +
                ':' + (data.seconds < 10 ? '0' + data.seconds : data.seconds);
        },
        filterDate: function (val) {
            return moment(val).format('HH:MM:SS');
        }
    },
    watch: {
        ticketsReports: function (val) {
            this.options.xaxis.categories = val.map(obj => obj.category);
            this.options.series[0].data = val.map(obj => obj.status[0].count)
            this.options.series[1].data = val.map(obj => obj.status[1].count)
            this.options.series[2].data = val.map(obj => obj.status[2].count)
            this.$refs.ticket.refresh();
        },
		districtId: function (val) {
			this.getStreets();
			this.streetId = null;
		},
		streetId: function (val) {
			this.getHouses();
			this.houseId = null;
		},
		houseId: function (val) {
			this.getFlats();
			this.flatId = null
		}
    }
}