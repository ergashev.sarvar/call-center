export default {
	RestoreTicketBatch(arr) {
		return new Promise((result, reject) => {
			axios
				.post('ClientCard/clientCardReOpen', arr)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},


	GetTickets(user_id, status_id, page) {
		return new Promise((result, reject) => {
			axios
				.get('ClientCard/userClientCardsByStatus/' + user_id + '/' + status_id + '/' + page)
				.then(response => {
					result(response.data);
				})
				.catch(error => {
					reject(error.message);
				});
		});
	},
}