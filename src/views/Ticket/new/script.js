import Paginate from 'vuejs-paginate';
import CRUD from "./CRUD";

export default {
	components: {
		Paginate,
	},

	data() {
		return {
			ticketList: [],
			ticketListPageCount: 0,


			checkBoxAllToggle: false,
			selectedTicketList: [],


			userList: [],
			categoryList: [],
			priorityList: [],
			statusList: [],
		}
	},


	methods: {
		DeleteTicketBatch() {
			if (confirm('You are about to delete the record! \nAre you sure?')) {
				CRUD.DeleteTicketBatch(this.selectedTicketList).then(result => {
					this.checkBoxAllToggle = false;
					this.selectedTicketList = [];
					this.ticketList = [];

					this.GetTickets();
					this.$emit('deleted');
					this.$store.state.notifyDelete();
				});
			}
		},
		GetTickets(page = 1) {
			CRUD.GetTickets(this.$store.state.user_id, 1, page).then(result => {
				// console.log(result);

				if (result.success) {
					this.userList = result.users;
					this.categoryList = result.categories;
					this.priorityList = result.priorities;
					this.statusList = result.statuses;

					this.ticketList = result.clientCards;
					this.ticketListPageCount = result.count;

					window.scroll({
						top: 0,
						left: 0,
						behavior: 'smooth'
					});
				}
				else {
					console.log(result.success);
				}
			});
		},


		GetCheckedItems(checked, id) {
			if (checked) {
				this.selectedTicketList.push(id);
			}
			else {
				this.selectedTicketList.splice(this.selectedTicketList.indexOf(id), 1);
			}
		},
	},


	mounted() {

		this.GetTickets();

	},


	watch: {
		'checkBoxAllToggle'(val) {
			if (val) {
				this.selectedTicketList = this.ticketList.map((val, idx, arr) => {
					return val.id;
				});
			}
			else {
				this.selectedTicketList = [];
			}
		}
	}
}