import Vue from 'vue'
import Vuex from 'vuex'
import VueSweetalert2 from 'vue-sweetalert2';
import locale from './locale';
Vue.use(VueSweetalert2);

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		username: localStorage.getItem('username') ? atob(localStorage.getItem('username')) : null,
		password: localStorage.getItem('password') ? atob(localStorage.getItem('password')) : null,
		caller_id: localStorage.getItem('caller_id'),
		operator_id: localStorage.getItem('operator_id'),
		user_id: localStorage.getItem('user_id'),
		organization_id: localStorage.getItem('organization_id'),
		call_center_status: localStorage.getItem('call_center_status') ? parseInt(localStorage.getItem('call_center_status')) : 2,
		call_terminated: false,
		baseURL: '',
		locale: {...locale},
		lang: 'ru',
		localization: localStorage.getItem('localization'),
		loader: false,
		crmPhone: null,
        isCalling: false,

		notifyEdit() {
			this.notify('Edited Successfully!!!', 'success');
		},
		notifyAdd() {
			this.notify('Added Successfully!!!', 'success');
		},
		notifyDelete() {
			this.notify('Deleted Successfully!!!', 'warning');
		},
		notifyGet() {
			this.notify('Got Successfully!!!', 'success');
		},
		notifySave() {
			this.notify('Saved Successfully!!!', 'success');
		},
		notifySend() {
			this.notify('Sent Successfully!!!', 'success');
		},
		notify(msg, type) {
			Vue.swal({
				title: msg,
				type: type
			});
		},
	},
	mutations: {
		setBaseUrl(state, url){
			state.baseURL = url
		},
		changeCrmPhone(state, number){
			state.crmPhone = number
		}
	},
	actions: {
	},
	getters: {
		can: state => (action) => {
			let permissions = JSON.parse(localStorage.getItem('permissions'));
			if (permissions) {
				permissions.forEach(item => {
					if (action === item.item) {
						return true;
					}
				});
			}
			return false;
		}
	}
})
