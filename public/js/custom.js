
/* Tooltip
============================================================*/
$('[data-toggle="tooltip"]').tooltip();
/*========== Tooltip ==========*/





/* Input Mask
============================================================*/
// Inputmask({
// 	mask: "+\\9\\98 (99) 999-99-99",
// }).mask(document.querySelectorAll(".inputmask"));
/*========== Input Mask ==========*/








/* Custom Tabs
============================================================*/
$(document).on('click', '[data-tab-btn]', function (e) {
	e.preventDefault();

	$(this).addClass('active').siblings().removeClass('active');
	$(this).closest('[data-tab]').find('[data-tab-pane]').removeClass('active').eq($(this).index()).addClass('active');
});
/*========== Custom Tabs ==========*/






/* Profile
============================================================*/
$(document).on('click', '.hd_profile > a', function (e) {
	e.preventDefault();

	$(this).parent().toggleClass('active');
});
/*========== Profile ==========*/






/* Contact Phone
============================================================*/
$(document).on('click', '.hd_contact_tel', function (e) {
	e.preventDefault();

	$(this).siblings('.hd_contact_dial_bx').toggleClass('active');
	setTimeout(function () {
		$('#dialField').focus();
	}, 500);
});
/*========== Contact Phone ==========*/









/* Remove Class when Out of Target
============================================================*/
$(document).on('click', '', function (e) {
	if ($(e.target).closest('.hd_contact_col').length === 0) {
		$('.hd_contact_dial_bx').removeClass('active');
	}

	if ($(e.target).closest('.hd_profile').length === 0) {
		$('.hd_profile').removeClass('active');
	}
});
/*========== Remove Class when Out of Target ==========*/














/* Header
============================================================*/

/*========== Header ==========*/










// crm page for right data


$(document).on('click','.add_right_databtn', function () {
	$(".ln_chatright").removeClass("right--350px");
});
$(document).on('click','.ln_chatright>.mdi-close', function () {
	$(".ln_chatright").addClass("right--350px");
});

