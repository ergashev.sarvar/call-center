
let
	/* Gulp
	----------------------------------------*/
	gulp = require('gulp'),


	/* Pug
	----------------------------------------*/
	// pug = require('gulp-pug'),


	/* SASS
	----------------------------------------*/
	sass = require('gulp-sass'),


	/* Auto Prefixer
	----------------------------------------*/
	autoprefixer = require('gulp-autoprefixer'),


	/* JavaScript
	----------------------------------------*/
	// uglify = require('gulp-uglify'),


	/* Rename
	----------------------------------------*/
	rename = require("gulp-rename"),


	/* Browser Sync
	----------------------------------------*/
	// browserSync = require('browser-sync').create(),


	/* Image
	----------------------------------------*/
	// imagemin = require('gulp-imagemin'),
	// cache = require('gulp-cache'),


	/* Delete
	----------------------------------------*/
	del = require('del');


	/* Run Sequence
	----------------------------------------*/
	// runSequence = require('run-sequence');









/* Error Handler
=================================================================*/
function ErrorHandler(err) {
	console.log(err.toString());
	this.emit('end');
}









/* Browser Sync
=================================================================*/
gulp.task('browserSync', function() {
	browserSync.init({
		server: {
			baseDir: "app"
		}
	});
});













/* Pug
=================================================================*/
gulp.task('pug', function () {
	return gulp.src('app/pug/*.pug')
		.pipe(
			pug({
				doctype: 'html',
				pretty: '\t'
			})
			.on('error', ErrorHandler)
		)
		.pipe(gulp.dest('app'));
});









/* Css Minimized
=================================================================*/
gulp.task('cssMin', function () {
	return gulp.src('public/scss/**/*.scss')
		.pipe(
			sass({
				outputStyle: 'compressed'
			})
			.on('error', ErrorHandler)
		)
		.pipe(
			autoprefixer({
				browsers: ['last 5 versions'],
				cascade: false
			})
		)
		.pipe(
			rename({
				suffix: ".min"
			})
		)
		.pipe(gulp.dest('public/css'));
});


/* Css Maximized
=================================================================*/
gulp.task('cssMax', function () {
	return gulp.src('app/scss/**/*.scss')
		.pipe(
			sass({
				outputStyle: 'expanded'
			})
			.on('error', ErrorHandler)
		)
		.pipe(
			autoprefixer({
				browsers: ['last 5 versions'],
				cascade: false
			})
		)
		.pipe(gulp.dest('app/css'));
});










/* Uglify
=================================================================*/
gulp.task('uglify', function(){
	return gulp.src(['app/js/**/*.js', '!app/js/**/*.min.js'])
		.pipe(
			uglify().on('error', ErrorHandler)
		)
		.pipe(
			rename({
				suffix: ".min"
			})
		)
		.pipe(gulp.dest('app/js'));
});










/* Image Minify
=================================================================*/
gulp.task('img', function(){
	return gulp.src('app/imgAll/**/*.+(png|jpg|JPG|jpeg|gif|svg)')
		.pipe(
			imagemin({
				verbose: true
			})
		)
		.pipe(gulp.dest('app/img'));
});











/* Delete
=================================================================*/
gulp.task('del', function() {
	return del.sync('dist');
});

/* Copy
=================================================================*/
gulp.task('copy', function() {
	return gulp.src([
			'app/css/**/*',
			'app/font/**/*',
			'app/img/**/*',
			'app/js/**/*',
			'app/lib/**/*',
			'app/*.html'
		],
		{base:"app"})
		.pipe(gulp.dest('dist'));
});

/* Build for Distribution
=================================================================*/
gulp.task('build', function(callback) {
	runSequence('uglify', 'img', 'del', 'copy', callback);
});










/* Watcher
=================================================================*/
gulp.task('watch', function () {
	// gulp.watch(['app/pug/**/*.pug', 'app/pug/**/*.html'], ['pug']);
	gulp.watch('public/scss/**/*.scss', ['cssMin']);
});


/* Watcher & Browser Sync
=================================================================*/
// gulp.task('watchBS', ['browserSync'], function () {
// 	// gulp.watch(['app/pug/**/*.pug', 'app/pug/**/*.html'], ['pug']);
// 	gulp.watch('app/scss/**/*.scss', ['cssMin']);
// 	gulp.watch('app/css/**/*.css', browserSync.reload);
//
// 	gulp.watch('app/js/**/*.js', browserSync.reload);
// 	gulp.watch('app/*.html', browserSync.reload);
// });









/* Default
=================================================================*/
gulp.task('default', ['watch']);

